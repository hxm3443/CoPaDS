package edu.rit.cs.gossip.heartbeat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the Active Peer in the Gossip Heartbeat Protocol.
 */
public class PeerHeartbeatClient extends PeerHeartbeat {

    /**
     * Select a neighboring peer to send a message
     *
     * @return Selected peer
     * */
    public Set<String> selectPeer() {
        Set<String> peersSelected = new HashSet<>();
        int peerAddressesSize = peerAddresses.size();
        int groupSize = peerAddressesSize == 0 ? 0 : random.nextInt(peerAddresses.size());
        int counter = 0;
        while (counter != groupSize) {
            int randIndex = random.nextInt(peerAddressesSize);
            String randPeerIpAddress = peerAddresses.get(randIndex);
            Boolean randPeerStatus = peerCache.get(randPeerIpAddress);
            if (Boolean.FALSE.equals(randPeerStatus)) {
                System.out.println("Current peer " + randPeerIpAddress + " is dead as per local " +
                        "cache therefore not adding to peers selected.");
                continue;
            }
            InetAddress inetAddress = null;
            try {
                inetAddress = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            String ownIpAddress = inetAddress.getHostAddress();
            if (randPeerIpAddress.equals(ownIpAddress)) {
                System.out.println("Current peer " + randPeerIpAddress + " is actually me! i.e. " + ownIpAddress);
                continue;
            }
            System.out.println("Current peer " + randPeerIpAddress + " is added to peers selected.");
            peersSelected.add(peerAddresses.get(randIndex));
            counter++;
        }
        System.out.println("Total " + counter + " peers have been selected.");
        return peersSelected;
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param ipAddress Selected peer
     * @param msg       Selected message
     */
    public Socket sendTo(String ipAddress, PeerMessage msg) {
        Socket s = null;
        ObjectOutputStream out;
        try {
            s = new Socket(ipAddress, 7896);
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(msg);
            System.out.println("The following message has been sent to " + ipAddress + " on the socket: " +
                    msg.getMessage() + " " + msg.getPeerCache().toString());
        }
        catch (ConnectException e) {
            this.peerCache.put(ipAddress, false);
            System.out.println("Found a dead connection on IP Address " + ipAddress);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * Receive a message from a peer
     *
     * @param s socket
     * @return PeerMessage
     */
    public PeerMessage receiveFrom(Socket s) {
        ObjectInputStream in;
        PeerMessage msg = null;
        try {
            System.out.println("Starting to receive peer msg on peer heartbeat client.");
            in = new ObjectInputStream(s.getInputStream());
            System.out.println("Completed receiving peer msg on the peer heartbeat client.");
            msg = (PeerMessage) in.readObject();
            System.out.println("Trying to fetch data " + msg.getMessage() +
                    " " + msg.getPeerCache());
            /*if (in.available() != 0) {
                msg = (PeerMessage) in.readObject();
                System.out.println("Found actual data " + msg.getMessage() +
                        " after receiving peer msg on the peer heartbeat client.");
                return msg;
            }*/
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Handling Exception: Peer " + s.getInetAddress().getHostAddress() + " has closed the connection.");
            return msg;
        }
        return msg;
    }
}
