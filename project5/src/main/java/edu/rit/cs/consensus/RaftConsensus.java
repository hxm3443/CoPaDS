package edu.rit.cs.consensus;

import java.net.InetAddress;

/**
 * This class implements Leader Election in RAFT Protocol.
 */
public class RaftConsensus {

    /**
     * Main entry point that instantiates Peer/Client as well as initiates all the threads.
     *
     * @param args manually assigned ip address and filename if Client, empty otherwise
     */
    public static void main(String[] args) {

        Peer peer = new Peer();
        ClientPeer clientPeer;
        TCPClientPeer tcpClientPeer;
        BroadcastingClient broadcastingClient = null;
        BroadcastingServer broadcastingServer = null;
        TCPClientHeartbeat tcpClientHeartbeat = null;
        TCPClientElection tcpClientElection = null;
        TCPServer tcpServer = null;

        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            String ipAddress = inetAddress.getHostAddress();
            if (args.length > 0) {
                System.out.println("[C] Client is up and running.");
                // args[0] contains a peer's manually assigned ipaddress
                clientPeer = new ClientPeer(ipAddress, args[0]);
                // args[1] contains filename
                tcpClientPeer = new TCPClientPeer(clientPeer, args[1]);
                tcpClientPeer.run();
            } else {
                broadcastingClient = new BroadcastingClient(peer, ipAddress);
                System.out.println("[T1] Gossip Heartbeat is starting the broadcasting client.");
                broadcastingClient.start();

                broadcastingServer = new BroadcastingServer();
                System.out.println("[T2] Gossip Heartbeat is starting the broadcasting server.");
                broadcastingServer.start();

                tcpClientElection = new TCPClientElection(peer);
                System.out.println("[T3] Gossip Heartbeat is starting the TCP Client Election.");
                tcpClientElection.start();

                tcpClientHeartbeat = new TCPClientHeartbeat(peer);
                System.out.println("[T4] Gossip Heartbeat is starting the TCP Client Heartbeat.");
                tcpClientHeartbeat.start();

                tcpServer = new TCPServer(peer);
                System.out.println("[T5] Gossip Heartbeat is starting the TCP Server Heartbeat.");
                tcpServer.start();
            }
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted, Failed to complete operation");
            if (tcpClientElection != null) {
                tcpClientElection.interrupt();
            }
            if (tcpServer != null) {
                System.out.println("TCP Server has been interrupted.");
                tcpServer.interrupt();
            }
            if (tcpClientHeartbeat != null) {
                System.out.println("TCP Client has been interrupted.");
                tcpClientHeartbeat.interrupt();
            }
            if (broadcastingClient != null) {
                System.out.println("Broadcasting Client has been interrupted.");
                broadcastingClient.interrupt();
            }
            if (broadcastingServer != null) {
                System.out.println("Broadcasting Server has been interrupted.");
                broadcastingServer.interrupt();
            }
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
