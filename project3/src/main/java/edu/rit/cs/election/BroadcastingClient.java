package edu.rit.cs.election;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * This class handles one-to-all (broadcast) communication between peers. It is used for peer discovery in the
 * RAFT protocol.
 */

public class BroadcastingClient extends Thread {
    private DatagramSocket socket;
    private InetAddress broadcastingAddress;
    private Peer peerClient;
    private String ipAddress;
    private byte[] buf;
    private boolean running;

    /**
     * Constructor for Broadcasting Client
     *
     * @param peerClient peer which joins the network
     * @param ipAddress  ipAddress of the peer
     * @throws Exception
     */
    public BroadcastingClient(Peer peerClient, String ipAddress) throws Exception {
        this.ipAddress = ipAddress;
        this.peerClient = peerClient;
        this.broadcastingAddress = InetAddress.getByName("255.255.255.255");
    }

    /**
     * This function is responsible for sending out PeerInfoMessage(electionTerm, ipAddress) of the peer on the
     * broadcast address.
     *
     * @param electionTerm Current election term of the peer which joins the network
     * @param ipAddress    ipAddress of the peer
     * @throws IOException
     */
    public void discoverPeers(Integer electionTerm, String ipAddress) throws IOException {
        initializeSocketForBroadcasting();
        copyMessageOnBuffer(electionTerm, ipAddress);
        broadcastPacket(broadcastingAddress);
    }

    /**
     * This function initializes the Datagram socket and sets the socket as available for broadcasting the packet.
     *
     * @throws SocketException
     */
    private void initializeSocketForBroadcasting() throws SocketException {
        socket = new DatagramSocket();
        socket.setBroadcast(true);
    }

    /**
     * This function converts the PeerInfoMessage into bytes and writes it to a buffer.
     *
     * @param electionTerm electionTerm of the peer
     * @param ipAddress    IP Address of the newly joined peer
     */
    private void copyMessageOnBuffer(Integer electionTerm, String ipAddress) {
        PeerInfoMessage infoMessage = new PeerInfoMessage(ipAddress, electionTerm);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(bos);
            oos.writeObject(infoMessage);
            oos.flush();
            buf = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function is responsible for broadcasting the packet on the broadcast address.
     *
     * @param broadcastingAddress Broadcast Address
     * @throws IOException
     */
    private void broadcastPacket(InetAddress broadcastingAddress) throws IOException {
        DatagramPacket packet = new DatagramPacket(buf, buf.length, broadcastingAddress, 7896);
        socket.send(packet);
    }

    /**
     * This function closes the socket.
     */
    public void close() {
        socket.close();
    }

    /**
     * This function broadcasts this peer's PeerInfoMessage(electionTerm, ipAddress) on the broadcast address.
     */
    public void run() {
        running = true;
        try {
            while (running) {
                discoverPeers(peerClient.getElectionTerm(), ipAddress);
            }
        } catch (IOException e) {
            e.printStackTrace();
            running = false;
        } finally {
            this.close();
        }
    }
}