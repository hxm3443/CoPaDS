package edu.rit.cs.consensus;

import java.io.Serializable;

/**
 * PeerInfoMessage object to store the IP Address and Election Term of the peer.
 */
public class PeerInfoMessage implements Serializable {
    String ipAddress;
    Integer electionTerm;

    public PeerInfoMessage(String ipAddress, Integer electionTerm) {
        this.ipAddress = ipAddress;
        this.electionTerm = electionTerm;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getElectionTerm() {
        return electionTerm;
    }

    public void setElectionTerm(Integer electionTerm) {
        this.electionTerm = electionTerm;
    }

}
