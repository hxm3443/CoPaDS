package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.net.Socket;
import java.util.*;

/**
 * TCP Client is the running implementation of the Active Peer in the Gossip Random Number Protocol.
 */
public class TCPClient extends Thread {
    private Boolean running;
    private PeerRandomNumClient peerRandomNumClient = new PeerRandomNumClient();
    private Map<Socket, PendingSocketInfo> pendingSocketConnections = new HashMap<>();

    /**
     * Checks if the IP Address is already present in the pending socket connections
     * @param ipAddress
     * @return
     */
    private boolean isIpAddressInPendingSocketConnection(String ipAddress) {
        // System.out.println("[T3] Checking if ip address " + ipAddress + " already has any pending connections.");
        for(PendingSocketInfo pendingSocketInfo : pendingSocketConnections.values()) {
            if (ipAddress.equals(pendingSocketInfo.getIpAddress())) {
                System.out.println("[T3] ip address " + ipAddress + " has a pending connection.");
                return true;
            }
        }
        System.out.println("[T3] ip address " + ipAddress + " does not have a pending connection.");
        return false;
    }

    /**
     * Initiates the active peer.
     */
    public void run() {
        running = true;
        int countdown = 15;
        int roundCounter = 0;
        while (running) {
            try {
                // System.out.println("[T3] TCP Client is starting peer selection.");
                Set<String> peersSelected = peerRandomNumClient.selectPeer();
                System.out.println("[T3] " + peersSelected.size() + " peers have been selected by TCP Client.");
                Map<Integer, List<String>> outgoingCache = peerRandomNumClient.selectToSend();
                System.out.println("[T3] Peer message: " + outgoingCache.toString() + " is ready to be sent by TCP Client.");

                for (String ipAddress : peersSelected) {
                    if(isIpAddressInPendingSocketConnection(ipAddress)) {
                        continue;
                    }
                    System.out.println("[T3] TCP Client is sending to ip address " + ipAddress);
                    Socket s = peerRandomNumClient.sendTo(ipAddress, outgoingCache);
                    if (s != null) {
                        PendingSocketInfo pendingSocketInfo = new PendingSocketInfo(ipAddress, System.currentTimeMillis());
                        pendingSocketConnections.put(s, pendingSocketInfo);
                    }
                }

                System.out.println("[T3] Starting to iterate over pending socket connections which has " + pendingSocketConnections.size()
                        + " connections.");
                Set<Socket> sockets = pendingSocketConnections.keySet();
                Set<Socket> socketsToBeRemoved = new HashSet<>();
                for (Socket s : sockets) {
                    System.out.println("INSIDE FOR LOOP");
                    Map<Integer, List<String>> incomingCache = peerRandomNumClient.receiveFrom(s);
                    System.out.println("[T3] TCP Client has received from pending socket connection: " + s.toString());
                    Long timestampDiffInSeconds = (System.currentTimeMillis() -  pendingSocketConnections.get(s).getTimestamp())/1000;
                    System.out.println("[T3] The timestamp diff in response for pending socket connection: "
                            + s.toString() + " is " + timestampDiffInSeconds);
                    if (timestampDiffInSeconds >= 10) {
                        System.out.println("[T3] Found a dead connection from socket: " + pendingSocketConnections.get(s).getIpAddress());
                        PeerStatus peerStatus = new PeerStatus(pendingSocketConnections.get(s).getIpAddress(), false);
                        peerRandomNumClient.alivePeers.add(peerStatus);
                        socketsToBeRemoved.add(s);
                        s.close();
                        continue;
                    }
                    else if (incomingCache == null) {
                        System.out.println("[T3] Socket : " + s.toString() + " has not received data yet however 10 seconds have not passed.");
                        continue;
                    }
                    PeerStatus peerStatus = new PeerStatus(s.getInetAddress().getHostAddress(), true);
                    peerRandomNumClient.alivePeers.add(peerStatus);
                    Integer incomingMax = incomingCache.keySet().iterator().next();
                    System.out.println("[T3] Socket : " + s.toString() + " has received data within 10 seconds.");
                    Boolean shouldKeepNum = peerRandomNumClient.selectToKeep(incomingMax);
                    if (Boolean.TRUE.equals(shouldKeepNum)) {
                        peerRandomNumClient.processData(incomingCache);
                    }
                    socketsToBeRemoved.add(s);
                    s.close();
                }
                // update pending socket connections
                for(Socket s : socketsToBeRemoved) {
                    pendingSocketConnections.remove(s);
                }
                System.out.println("Successfully removed " + socketsToBeRemoved.size() + " sockets from pending socket connections.");
                // System.out.println("[T3] TCP Client is sleeping for 1 second.");
                Thread.sleep(1000);
                countdown--;
                if (countdown == 0) {
                    System.out.println("Current countdown: " + countdown);
                    roundCounter++;
                    Map<Integer, List<String>> peerCache = peerRandomNumClient.getPeerCache();
                    Integer finalMaxForTheRound = peerCache.keySet().iterator().next();
                    List<String> peersWithMax = peerCache.get(finalMaxForTheRound);
                    System.out.println("Round " + roundCounter + ":");
                    for (String peerAddress : peersWithMax) {
                        System.out.println("Peer " + peerAddress + " , " + "Max Number: " + String.valueOf(finalMaxForTheRound));
                    }
                    peerRandomNumClient = new PeerRandomNumClient();
                    System.out.println("Resetting the game. New Current max is " + peerRandomNumClient.currentMax);
                    countdown = 15;
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}
