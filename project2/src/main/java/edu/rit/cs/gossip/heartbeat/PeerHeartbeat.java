package edu.rit.cs.gossip.heartbeat;

import java.util.*;

/**
 * Implementation of a Peer in the Gossip Heartbeat Protocol
 */
public class PeerHeartbeat {
    protected static final String ALIVE_MSG = "I am alive.";
    final protected static List<String> peerAddresses = new ArrayList<>();
    final protected Map<String, Boolean> peerCache;
    protected Random random;

    /**
     * Constructor for PeerHeartbeat
     */
    public PeerHeartbeat() {
        this.peerCache = new HashMap<>();
        this.random = new Random();
    }

    /**
     * Getter for the peer cache
     *
     * @return peerCache
     */
    public Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    /**
     * Select a message to be sent
     *
     * @return Selected message
     */
    public PeerMessage selectToSend() {
        return new PeerMessage(ALIVE_MSG, peerCache);
    }

    /**
     * Process the data from a local cache
     *
     * @param incomingCache The map containing ipAddress and status for peers.
     */
    public void processData(Map<String, Boolean> incomingCache) {
        synchronized (peerCache) {
            System.out.println("Starting to process the incoming cache.");
            for (String key : incomingCache.keySet()) {
                peerCache.put(key, incomingCache.get(key));
            }
            System.out.println("Completed processing the incoming cache.");
        }
    }

    /**
     * Determine whether to cache the message into peer cache.
     *
     * @param ipAddress IP Address
     */
    public void selectToKeep(String ipAddress) {
        synchronized (peerCache) {
            System.out.println("Adding the following ip address to the peer cache: " + ipAddress);
            peerCache.put(ipAddress, true);
        }
    }
}
