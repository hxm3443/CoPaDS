package edu.rit.cs.socket;

import java.net.*;
import java.io.*;

public class TCPClient {

    private static final String OUTPUT_RECV_FILE = "output_recv.txt";
    private static final byte EOF = '\0';

    public static void main(String args[]) {
        // arguments supply filename and hostname of destination
        String datasetFile = args[0];
        String serverAddress = args[1];

        DataInputStream in = null;
        DataOutputStream out = null;
        Socket s = null;
        try {
            int serverPort = 7896;
            s = new Socket(serverAddress, serverPort);

            in = new DataInputStream(s.getInputStream());
            out = new DataOutputStream(s.getOutputStream());

            // Read and Send the file from Client to Server
            System.out.println("Ready to read and send data to the server.");
            FileInputStream fis = new FileInputStream(datasetFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            DataInputStream dis = new DataInputStream(bis);

            byte buffer[] = new byte[1000];
            int read;
            System.out.println("Start sending the file data to the server.");
            while ((read = dis.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            out.write(EOF);

            fis.close();
            bis.close();
            dis.close();
            System.out.println("Done sending the file data.");


            //Receive and save the output file from the server
            System.out.println("Starting to receive and write the data to " + OUTPUT_RECV_FILE);
            FileOutputStream fos = new FileOutputStream(OUTPUT_RECV_FILE);
            byte[] buf = new byte[1000];
            int r;
            while((r = in.read(buf)) != -1){
                if(buf[r - 1] == EOF) {
                    fos.write(buf, 0, r - 1);
                    break;
                }
                fos.write(buf, 0, r);
            }
            fos.close();
            System.out.println("Done writing the result to " + OUTPUT_RECV_FILE);

            // Read the result file and print it on the terminal
            System.out.println("Starting to read " + OUTPUT_RECV_FILE + " and print it to the terminal.");
            BufferedReader br = new BufferedReader(new FileReader(OUTPUT_RECV_FILE));
            String reviewLine;

            while((reviewLine = br.readLine()) != null) {
                System.out.println(reviewLine);
            }
            System.out.println("Done printing the result file onto the terminal.");
            br.close();

        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (s != null)
                try {
                    s.close();
                } catch (IOException e) {
                    System.out.println("close:" + e.getMessage());
                }
        }
    }
}

