package edu.rit.cs.replication;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * TCP Server is the running implementation of the PeerServer in the Raft Protocol.
 */
public class TCPServer extends Thread {
    private Boolean running;
    private Peer peerServer;

    public TCPServer(Peer peerServer) {
        this.peerServer = peerServer;
    }

    /**
     * Initiates the thread responsible for handling VoteRequest, AliveMessages or Client Request Messages received by a
     * Peer.
     */
    public void run() {
        running = true;
        ServerSocket s = peerServer.receiveFromAny();
        Socket clientSocket = null;
        ObjectInputStream in;
        BufferedWriter bw = null;
        BufferedReader br = null;
        File finalFile = new File(Peer.FINAL_CACHE_FILE_NAME);
        File tempFile = new File(Peer.TEMP_CACHE_FILE_NAME);
        while (running) {
            try {
                System.out.println("Current State: " + peerServer.getState());
                if (s != null) {
                    clientSocket = s.accept();
                    in = new ObjectInputStream(clientSocket.getInputStream());
                    PeerMessage receivedMsg = (PeerMessage) in.readObject();

                    if (receivedMsg.getKey().equals(Peer.REQUEST_VOTE_MSG)) {

                        // If IncomingElectionTerm > CurrentElectionTerm
                        // If IncomingElectionTerm = CurrentElectionTerm
                        // If IncomingElectionTerm < CurrentElectionTerm
                        String voteToBeSent = "";
                        if (receivedMsg.getValue() > peerServer.getElectionTerm()) {
                            voteToBeSent = Peer.VOTE_MSG;
                            peerServer.setState("FOLLOWER");
                            peerServer.resetNumVotes();
                            peerServer.setElectionTerm(receivedMsg.getValue());
                            peerServer.setPotentialCandidate(false);
                        } else if (receivedMsg.getValue().equals(peerServer.getElectionTerm())) {
                            if (peerServer.getState().equals("FOLLOWER") && !peerServer.isHasVoted()) {
                                voteToBeSent = Peer.VOTE_MSG;
                            } else {
                                voteToBeSent = Peer.NO_VOTE_MSG;
                            }
                        } else {
                            voteToBeSent = Peer.VOTE_MSG;
                        }
                        PeerMessage voteMessage = peerServer.selectVoteMessageToSend(voteToBeSent);
                        peerServer.sendTo(clientSocket, voteMessage);
                        peerServer.setHasVoted(true);
                    } else if (receivedMsg.getKey().equals(Peer.ALIVE_MSG)) {
                        int currentElectionTerm = peerServer.getElectionTerm();
                        int incomingElectionTerm = receivedMsg.getValue();
                        //CASE 1: Incoming election term > current election term - update term and state
                        //CASE 2: Incoming election term = current election term - update state
                        //CASE 3: Incoming election term < current election term - discard msg
                        boolean receivedMsgFromTrueLeader = false;
                        if (incomingElectionTerm > currentElectionTerm) {
                            peerServer.setElectionTerm(incomingElectionTerm);
                            peerServer.setState("FOLLOWER");
                            receivedMsgFromTrueLeader = true;
                        } else if (incomingElectionTerm == currentElectionTerm) {
                            peerServer.setState("FOLLOWER");
                            receivedMsgFromTrueLeader = true;
                        }

                        if (receivedMsgFromTrueLeader) {
                            peerServer.setLeaderLastTimeStamp(System.currentTimeMillis());
                            peerServer.setLeaderIpAddress(clientSocket.getInetAddress().getHostAddress());
                            peerServer.setReceivedLeaderMsg(true);
                            peerServer.setPotentialCandidate(false);
                            PeerMessage sendMessage = peerServer.selectToSend();
                            peerServer.sendTo(clientSocket, sendMessage);
                        } else {
                            PeerMessage sendMessage = peerServer.selectToSend();
                            peerServer.sendTo(clientSocket, sendMessage);
                        }
                    } else if (receivedMsg.getKey().equals(ClientPeer.REQUEST_LEADER)) {
                        PeerMessage sendMessage = new PeerMessage(peerServer.getLeaderIpAddress(), null,
                                null, null);
                        peerServer.sendTo(clientSocket, sendMessage);
                    } else if (receivedMsg.getKey().equals(Peer.COMMIT_MSG)) {
                        // Peer stores info in its final cache file
                        String lastLineRead = "";

                        // Storing the last line of the file
                        br = new BufferedReader(new FileReader(Peer.TEMP_CACHE_FILE_NAME));
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (line.equals("\n")) {
                                break;
                            }
                            lastLineRead = line;
                        }
                        br.close();

                        String fileContent = lastLineRead + "\n";
                        // checks if file exists; creates one if doesn't exist
                        if (!finalFile.exists()) {
                            finalFile.createNewFile();
                        }
                        // process line based on action
                        FileWriter fw = new FileWriter(finalFile, true);
                        bw = new BufferedWriter(fw);
                        bw.write(fileContent);
                        bw.flush();

                        // update/store/remove/retrieve the dataCache
                        String[] splitLastLine = lastLineRead.split(" ");
                        String action = splitLastLine[0];
                        String key = splitLastLine[1];
                        Integer value = null;
                        if (!action.equals("RETRIEVE")) {
                            value = Integer.parseInt(splitLastLine[2]);
                        }

                        if (action.equals("UPDATE") && peerServer.dataCache.containsKey(key)) {
                            // UPDATE
                            peerServer.dataCache.replace(key, value);
                        } else if (action.equals("STORE") && !peerServer.dataCache.containsKey(key)) {
                            // STORE
                            peerServer.dataCache.put(key, value);
                        } else if (action.equals("DELETE")) {
                            // REMOVE
                            peerServer.dataCache.remove(key, value);
                        } else if (action.equals("RETRIEVE")) {
                            // RETRIEVE
                            peerServer.dataCache.get(key);
                        } else {
                            System.out.println("INCORRECT ACTION");
                        }
                        System.out.println("PEER DATA CACHE: " + peerServer.dataCache.toString());

                        // Send response back to the leader
                        PeerMessage msgToSend = new PeerMessage(Peer.PEER_DONE_WRITING, null, null, null);
                        System.out.println("[C3] PEER DONE WRITING ON SERVER");
                        peerServer.setLeaderLastTimeStamp(System.currentTimeMillis());
                        peerServer.setLeaderIpAddress(clientSocket.getInetAddress().getHostAddress());
                        peerServer.setReceivedLeaderMsg(true);
                        peerServer.setPotentialCandidate(false);
                        peerServer.sendTo(clientSocket, msgToSend);

                    } else if (receivedMsg.getAction() != null) {
                        // Received Data Message from Client Peer

                        if (peerServer.getState().equals("LEADER")) {
                            peerServer.clientIpAddress = clientSocket.getInetAddress().getHostAddress();
                            peerServer.clientPeerSocket = clientSocket;
                            System.out.println("[C3] Client sent a request to the Leader on the Server");
                        }

                        peerServer.hasReceivedRequest = true;

                        // Leader stores info in its temp cache file
                        // Follower/Candidate stores info in its temp cache file + send back to leader
                        String fileContent;
                        if (receivedMsg.getAction().equals("RETRIEVE")) {
                            fileContent = receivedMsg.getAction() + " " + receivedMsg.getKey() + "\n";
                        } else {
                            fileContent = receivedMsg.getAction() + " " + receivedMsg.getKey() + " " +
                                    receivedMsg.getValue() + "\n";
                        }


                        // checks if file exists; creates one if doesn't exist
                        if (!tempFile.exists()) {
                            tempFile.createNewFile();
                        }

                        bw = new BufferedWriter(new FileWriter(tempFile, true));
                        bw.write(fileContent);
                        //bw.close();
                        bw.flush();

                        if (peerServer.getState().equals("FOLLOWER") || peerServer.getState().equals("CANDIDATE")) {
                            PeerMessage msgToSend = peerServer.selectRequestReceived();
                            System.out.println("[C3] Leader sent client request to Follower/Candidate on the Server");
                            peerServer.setLeaderLastTimeStamp(System.currentTimeMillis());
                            peerServer.setLeaderIpAddress(clientSocket.getInetAddress().getHostAddress());
                            peerServer.setReceivedLeaderMsg(true);
                            peerServer.setPotentialCandidate(false);
                            peerServer.sendTo(clientSocket, msgToSend);
                        }
                    }
                }
            } catch (EOFException e) {
                System.out.println("No info available to read.");
                try {
                    if (clientSocket != null) {
                        clientSocket.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                running = true;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                running = false;
            } finally {
                // Timer to check if this peer hasn't heard from the leader for 10 secs
                if (!peerServer.getState().equals("LEADER")) {
                    if (((System.currentTimeMillis() - peerServer.getLeaderLastTimeStamp()) / 1000) >= 10) {
                        System.out.println("Haven't heard from leader in over 10 seconds. Updating leader info.");
                        List<PeerInfoMessage> peerInfoMessages = Peer.peerInfo;
                        for (PeerInfoMessage infoMessage : peerInfoMessages) {
                            if (infoMessage.getIpAddress().equals(peerServer.getLeaderIpAddress())) {
                                synchronized (Peer.peerInfo) {
                                    Peer.peerInfo.remove(infoMessage);
                                }
                                break;
                            }
                        }
                        peerServer.setLeaderLastTimeStamp(0L);
                        peerServer.setLeaderIpAddress("");
                        peerServer.setReceivedLeaderMsg(false);
                        peerServer.setPotentialCandidate(true);
                        peerServer.setTie(false);
                    }
                }
                //System.out.println("[T5] Current Term " + peerServer.getElectionTerm() + " LEADER " + peerServer.getLeaderIpAddress());

                try {
                    if (bw != null)
                        bw.close();
                } catch (Exception ex) {
                    System.out.println("Error in closing the BufferedWriter" + ex);
                }
            }
        }
    }
}
