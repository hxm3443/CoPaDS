package edu.rit.cs.replication;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * TCP Client Heartbeat is the running implementation of the Leader in the Raft Protocol.
 */
public class TCPClientHeartbeat extends Thread {
    private Boolean running;
    private Peer peerClient;

    public TCPClientHeartbeat(Peer peerClient) {
        this.peerClient = peerClient;
    }

    /**
     * Initiates the thread responsible for handling the actions performed by the Leader peer.
     */
    public void run() {
        File file = new File(Peer.FINAL_CACHE_FILE_NAME);
        running = true;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
            while (running) {
                if (peerClient.getState().equals("LEADER")) {
                    Set<Socket> peerConnections = new HashSet<>();
                    Set<String> peersSelected = peerClient.selectPeers();

                    // If a Client has sent a request to the Leader, it passes that to other peers. If the Leader receives
                    // a majority of votes, processes the request, commits the request, sends an acknowledgement back
                    // to the client, and asks other peers to commit the change in their respective log files.
                    if (peerClient.hasReceivedRequest) {
                        peerConnections = new HashSet<>();
                        String lastLineRead = "";

                        // Storing the last line of the file
                        BufferedReader br = new BufferedReader(new FileReader(Peer.TEMP_CACHE_FILE_NAME));
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (line.equals("\n")) {
                                break;
                            }
                            lastLineRead = line;
                        }
                        br.close();
                        String[] splitLastLine = lastLineRead.split(" ");
                        PeerMessage msgToSend;
                        if (splitLastLine[0].equals("RETRIEVE")) {
                            msgToSend = new PeerMessage(splitLastLine[1], splitLastLine[0], null, null);
                        } else {
                            msgToSend = new PeerMessage(splitLastLine[1], splitLastLine[0],
                                    Integer.parseInt(splitLastLine[2]), null);
                        }

                        for (String ipAddress : peersSelected) {
                            Socket socket = peerClient.sendTo(ipAddress, msgToSend);

                            if (socket != null) {
                                peerConnections.add(socket);
                            }
                        }

                        int yesVotes = 0;
                        int votesCollected = 0;
                        for (Socket s : peerConnections) {
                            PeerMessage msg = peerClient.receiveFrom(s);

                            if (msg == null) {
                                votesCollected++;
                                s.close();
                                continue;
                            }
                            yesVotes++;
                            votesCollected++;
                            s.close();
                        }
                        double voteRatio = (yesVotes * 1.0) / votesCollected;

                        if (voteRatio > 0.5) {
                            // store to final cache file
                            String fileContent = lastLineRead + "\n";
                            // checks if file exists; creates one if doesn't exist
                            if (!file.exists()) {
                                file.createNewFile();
                            }
                            // process line based on action
                            bw.write(fileContent);
                            bw.flush();

                            // update/store/remove/retrieve the dataCache
                            String action = splitLastLine[0];
                            String key = splitLastLine[1];
                            Integer value = null;
                            if (!action.equals("RETRIEVE")) {
                                value = Integer.parseInt(splitLastLine[2]);
                            }

                            Integer actionType;
                            Integer sendValToClient = null;
                            if (action.equals("UPDATE") && peerClient.dataCache.containsKey(key)) {
                                // UPDATE
                                actionType = 1;
                                peerClient.dataCache.replace(key, value);
                            } else if (action.equals("STORE") && !peerClient.dataCache.containsKey(key)) {
                                // STORE
                                actionType = 1;
                                peerClient.dataCache.put(key, value);
                            } else if (action.equals("DELETE")) {
                                // REMOVE
                                actionType = 1;
                                peerClient.dataCache.remove(key, value);
                            } else if (action.equals("RETRIEVE")) {
                                // RETRIEVE
                                actionType = 2;
                                sendValToClient = peerClient.dataCache.get(key);
                            } else {
                                actionType = 3;
                                System.out.println("INCORRECT ACTION");
                            }
                            System.out.println("LEADER DATA CACHE: " + peerClient.dataCache.toString());
                            // send response back to client
                            PeerMessage msgToClient = null;
                            if (actionType == 1) {
                                msgToClient = new PeerMessage(Peer.DONE_REQUEST, null, null, null);
                            } else if (actionType == 2) {
                                msgToClient = new PeerMessage(Peer.DONE_REQUEST, null, sendValToClient, null);
                            } else if (actionType == 3) {
                                System.out.println("INCORRECT ACTION; COULD NOT PROCESS REQUEST");
                            }

                            if (actionType != 3) {
                                // check if client does any processing on receiving the done message
                                peerClient.sendTo(peerClient.clientPeerSocket, msgToClient);
                                System.out.println("Done processing request and sent message to client: " +
                                        msgToClient.toString());
                            }

                            // send message to other peers to store in their respective final caches
                            PeerMessage msgToPeer = new PeerMessage(Peer.COMMIT_MSG, null, null,
                                    null);
                            peerConnections = new HashSet<>();

                            for (String ipAddress : peersSelected) {
                                Socket socket = peerClient.sendTo(ipAddress, msgToPeer);

                                if (socket != null) {
                                    peerConnections.add(socket);
                                }
                            }

                            // check if this threads needs to sleep before receiving
                            for (Socket s : peerConnections) {
                                PeerMessage msg = peerClient.receiveFrom(s);

                                if (msg == null) {
                                    s.close();
                                    continue;
                                }
                                s.close();
                            }
                        } else {
                            PeerMessage message = new PeerMessage(Peer.NO_MAJORITY, null, null,
                                    null);
                            peerClient.sendTo(peerClient.clientPeerSocket, message);
                        }

                        peerClient.hasReceivedRequest = false;
                    } else if (!peerClient.hasReceivedRequest) {
                        PeerMessage peerMessage = peerClient.selectToSend();

                        for (String ipAddress : peersSelected) {
                            Socket socket = peerClient.sendTo(ipAddress, peerMessage);

                            if (socket != null) {
                                peerConnections.add(socket);
                            }
                        }

                        for (Socket s : peerConnections) {
                            PeerMessage msg = peerClient.receiveFrom(s);

                            if (msg == null) {
                                s.close();
                                continue;
                            }

                            /*System.out.println("[T4] Leader received the message " + msg.getKey() +
                                    " for election term " + msg.getValue());*/
                            s.close();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            running = false;
        }
    }
}
