package edu.rit.cs.consensus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.*;


/**
 * Implementation of a Peer in the Raft Protocol. A Peer could represent any one of the three states: Leader, Candidate,
 * or Follower.
 */
public class Peer {
    protected String state;
    protected Integer electionTerm;
    protected int numVotes;
    protected boolean isTie;
    protected boolean hasVoted;
    protected String leaderIpAddress;
    String ipAddress;
    String clientIpAddress;
    String disconnectedPeerAddress;
    Socket clientPeerSocket;
    protected Long leaderLastTimeStamp;
    protected boolean isPotentialCandidate;
    protected boolean receivedLeaderMsg;
    boolean hasReceivedRequest;
    protected static final String REQUEST_VOTE_MSG = "Requesting for a vote!";
    protected static final String VOTE_MSG = "Vote";
    protected static final String NO_VOTE_MSG = "No Vote";
    protected static final String ALIVE_MSG = "I am alive.";
    Integer lineCount = 0;
    final static String DONE_REQUEST = "Done processing request.";
    final static String COMMIT_MSG = "Commit message";
    final static String PEER_DONE_WRITING = "Done Writing to the final cache.";
    final static String NO_MAJORITY = "No majority";
    final static String DISCONNECT_PEER = "Disconnected Peer";
    final protected static List<PeerInfoMessage> peerInfo = new ArrayList<>();
    final static Set<String> disconnectedPeers = new HashSet<>();
    final protected Map<String, Boolean> peerCache;
    final Map<String, Integer> dataCache;
    final static String TEMP_CACHE_FILE_NAME = "tempCache.txt";
    final static String FINAL_CACHE_FILE_NAME = "finalCache.txt";
    final static String REQUEST_RECEIVED_MSG = "Received Request";
    //static File finalCache = new File("finalCache.txt");
    protected Random random;

    /**
     * Constructor for Peer
     */
    public Peer() {
        this.state = "FOLLOWER";
        this.electionTerm = 0;
        this.numVotes = 0;
        this.leaderLastTimeStamp = 0L;
        this.isTie = false;
        this.hasVoted = false;
        this.isPotentialCandidate = true;
        this.receivedLeaderMsg = false;
        this.hasReceivedRequest = false;
        this.leaderIpAddress = "";
        this.clientIpAddress = "";
        this.disconnectedPeerAddress = "";
        try {
            this.ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            System.out.println("Unknown Host while creating a Peer.");
        }
        this.clientPeerSocket = null;
        this.peerCache = new HashMap<>();
        this.dataCache = new HashMap<>();
        this.random = new Random();
    }

    /**
     * Getter for Leader last time stamp
     *
     * @return last time stamp when a message was received from the Leader
     */
    public synchronized Long getLeaderLastTimeStamp() {
        return leaderLastTimeStamp;
    }

    /**
     * Sets Leader last time stamp
     *
     * @param leaderLastTime last time stamp when a message was received  from the Leader
     */
    public synchronized void setLeaderLastTimeStamp(Long leaderLastTime) {
        leaderLastTimeStamp = leaderLastTime;
    }

    /**
     * Getter for Leader Ip Address
     *
     * @return ip address of the Leader
     */
    public synchronized String getLeaderIpAddress() {
        return leaderIpAddress;
    }

    /**
     * Sets Leader Ip Address
     *
     * @param leaderIpAddr ip address of the Leader
     */
    public synchronized void setLeaderIpAddress(String leaderIpAddr) {
        leaderIpAddress = leaderIpAddr;
    }

    /**
     * Getter for whether it's a tie
     *
     * @return true if a split vote has occurred, false otherwise
     */
    public synchronized boolean isTie() {
        return isTie;
    }

    /**
     * Sets to true if a split vote occurred
     *
     * @param tie checks if split vote took place
     */
    public synchronized void setTie(boolean tie) {
        isTie = tie;
    }

    /**
     * Getter for isPotentialCandidate
     *
     * @return isPotentialCandidate
     */
    public synchronized boolean isPotentialCandidate() {
        return isPotentialCandidate;
    }

    /**
     * Setter for setPotentialCandidate
     *
     * @param potentialCandidate sets the peer as a potential candidate (true) if it has not heard from any candidate
     *                           before its electionTime runs out otherwise not (false)
     */
    public synchronized void setPotentialCandidate(boolean potentialCandidate) {
        isPotentialCandidate = potentialCandidate;
    }

    /**
     * Gets the state of hasVoted
     *
     * @return true if the peer has voted
     */
    public synchronized boolean isHasVoted() {
        return hasVoted;
    }

    /**
     * Setter for hasVoted
     *
     * @param hasVotedIn sets it to true if the peer has voted for either itself or for another peer
     */
    public synchronized void setHasVoted(boolean hasVotedIn) {
        hasVoted = hasVotedIn;
    }

    /**
     * Resets number of votes to 0 for a new election
     */
    public synchronized void resetNumVotes() {
        numVotes = 0;
    }

    /**
     * Increments number of votes as the peer receives vote messages from other peers
     */
    public synchronized void incrementNumVotes() {
        numVotes += 1;
    }

    /**
     * Getter for electionTerm
     *
     * @return election term of this peer
     */
    public synchronized Integer getElectionTerm() {
        return electionTerm;
    }

    /**
     * Setter for electionTerm
     *
     * @param electionTermIn sets the election term of this peer
     */
    public synchronized void setElectionTerm(Integer electionTermIn) {
        electionTerm = electionTermIn;
    }

    /**
     * Getter for the state of the peer
     *
     * @return one of three states: Leader, Candidate, or Follower
     */
    public synchronized String getState() {
        return state;
    }

    /**
     * Setter for the state of peer
     *
     * @param stateIn sets the state as one of the following: Leader, Candidate, or Follower
     */
    public synchronized void setState(String stateIn) {
        state = stateIn;
    }

    /**
     * Getter for Peer's IpAddress
     *
     * @return ipAddress of this peer
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Peer selects Vote Message to vote for a candidate if haven't voted already
     * Send a NO VOTE if you are also a candidate or you have voted already; otherwise
     * send VOTE if you haven't already voted
     *
     * @return PeerMessage containing vote, election term, and a null cache
     */
    public synchronized PeerMessage selectVoteMessageToSend(String voteMsg) {
        return new PeerMessage(voteMsg, null, electionTerm, null, null, null);
    }

    /**
     * Candidate requests a Vote Message from all peers
     *
     * @param electionTerm electionTerm of the candidate peer
     * @return PeerMessage containing voteRequest, candidate's election term, and a null cache
     */
    public synchronized PeerMessage selectVoterMessageToSend(Integer electionTerm) {
        return new PeerMessage(REQUEST_VOTE_MSG, null, electionTerm, null, null, null);
    }

    /**
     * Select a message to be sent
     *
     * @return Selected message
     */
    public synchronized PeerMessage selectToSend(List<String> fileContent) {
        return new PeerMessage(ALIVE_MSG, null, electionTerm, peerCache, fileContent, this.lineCount);
    }

    /**
     * Select Request Received Message to be sent
     *
     * @return Selected message
     */
    public synchronized PeerMessage selectRequestReceived() {
        return new PeerMessage(REQUEST_RECEIVED_MSG, null, null, null, null, null);
    }

    /**
     * Sets Received Leader Msg
     *
     * @param receivedLeaderMsgIn true if a message is received from the Leader
     */
    public synchronized void setReceivedLeaderMsg(boolean receivedLeaderMsgIn) {
        receivedLeaderMsg = receivedLeaderMsgIn;
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param s   Selected peer
     * @param msg Selected message
     */
    public synchronized void sendTo(Socket s, PeerMessage msg) {
        ObjectOutputStream out;
        try {
            if (s.isConnected()) {
                out = new ObjectOutputStream(s.getOutputStream());
                out.writeObject(msg);
            } else {
                disconnectedPeers.add(s.getInetAddress().getHostAddress());
            }
        } catch (NoRouteToHostException e) {
            System.out.println("1. Host has been disconnected.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a message from any peer
     *
     * @return ServerSocket
     */
    public synchronized ServerSocket receiveFromAny() {
        ServerSocket listenSocket = null;
        try {
            int serverPort = 7896;
            listenSocket = new ServerSocket(serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listenSocket;
    }

    /**
     * Selects all the peers who joined the network to send voteRequests or aliveMessages
     *
     * @return a Set containing all peers from the PeerInfo list
     */
    public synchronized Set<String> selectPeers() {
        Set<String> peersSelected = new HashSet<>();
        try {
            System.out.println(InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            this.disconnectedPeerAddress = this.ipAddress;
            System.out.println("Host has been disconnected from the network.");
        }

        String peerAddress;
        // Sending to all peers except for itself
        for (PeerInfoMessage msg : peerInfo) {
            peerAddress = msg.getIpAddress();
            if (!peerAddress.equals(this.ipAddress)) {
                peersSelected.add(peerAddress);
            }
        }

        return peersSelected;
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param ipAddress Selected peer
     * @param msg       Selected message
     */
    public synchronized Socket sendTo(String ipAddress, PeerMessage msg) {
        Socket s = null;
        ObjectOutputStream out;
        try {
            s = new Socket(ipAddress, 7896);
            if (s != null) {
                System.out.println("Peer msg is " + msg.getValue() + " : " + msg.getKey());
                out = new ObjectOutputStream(s.getOutputStream());
                out.writeObject(msg);
            } else {
                disconnectedPeers.add(ipAddress);
            }
        } catch (NoRouteToHostException e) {
            disconnectedPeers.add(ipAddress);
            List<PeerInfoMessage> localPeerInfo = new ArrayList<>(peerInfo);
            for (PeerInfoMessage peerInfoMessage : localPeerInfo) {
                if (peerInfoMessage.getIpAddress().equals(ipAddress)) {
                    synchronized (peerInfo) {
                        peerInfo.remove(peerInfoMessage);
                        System.out.println("Removed disconnected peer.");
                    }
                }
            }
            if (s != null) {
                try {
                    s.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            System.out.println("6. Host has been disconnected.");
        } catch (ConnectException e) {
            this.peerCache.put(ipAddress, false);
            System.out.println("Found a dead connection on IP Address " + ipAddress);
        } catch (SocketException e) {
            System.out.println("7. No socket connection with host.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * Receive a message from a peer
     *
     * @param s socket
     * @return PeerMessage
     */
    public synchronized PeerMessage receiveFrom(Socket s) {
        ObjectInputStream in;
        PeerMessage msg = null;
        try {
            if (s.isConnected()) {
                in = new ObjectInputStream(s.getInputStream());
                msg = (PeerMessage) in.readObject();
            } else {
                disconnectedPeers.add(s.getInetAddress().getHostAddress());
            }
        } catch (NoRouteToHostException e) {
            System.out.println("3. Host has been disconnected.");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Handling Exception: Peer " + s.getInetAddress().getHostAddress() + " has closed the connection.");
            return msg;
        }
        return msg;
    }
}
