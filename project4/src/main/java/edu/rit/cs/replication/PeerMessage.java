package edu.rit.cs.replication;

import java.io.Serializable;
import java.util.Map;

/**
 * PeerMessage object to store the IP Address/Message, Action (if any), Election Term/Value, and Peer Cache.
 */
public class PeerMessage implements Serializable {
    private String key;     // represents key for a peer/key for a client peer
    private String action;
    private Integer value;    // represents election term for a peer/value for a client peer
    private Map<String, Boolean> peerCache;

    public PeerMessage(String key, String action, Integer value, Map<String, Boolean> peerCache) {
        this.key = key;
        this.action = action;
        this.value = value;
        this.peerCache = peerCache;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    public void setPeerCache(Map<String, Boolean> peerCache) {
        this.peerCache = peerCache;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
