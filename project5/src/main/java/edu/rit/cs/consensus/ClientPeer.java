package edu.rit.cs.consensus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.Socket;

/**
 * Implementation of a Client in the Raft Protocol
 */

public class ClientPeer {
    final static String REQUEST_LEADER = "REQUEST LEADER";
    String clientIpAddress;
    String leaderIpAddress;

    public ClientPeer(String clientIpAddress, String leaderIpAddress) {
        this.clientIpAddress = clientIpAddress;
        this.leaderIpAddress = leaderIpAddress;
    }

    /**
     * Select a message to be sent
     *
     * @return Selected message
     */
    public synchronized PeerMessage selectLeaderRequest() {
        return new PeerMessage(REQUEST_LEADER, null, null, null, null, null);
    }

    /**
     * Select a message to be sent
     *
     * @return Selected message
     */
    public synchronized PeerMessage selectSendData(String action, String key, Integer value) {
        return new PeerMessage(key, action, value, null, null, null);
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param peerIpAddress Selected peer
     * @param msg           Selected message
     */
    public synchronized Socket sendTo(String peerIpAddress, PeerMessage msg) {
        Socket s = null;
        ObjectOutputStream out;
        try {
            s = new Socket(peerIpAddress, 7896);
            if (s != null) {
                out = new ObjectOutputStream(s.getOutputStream());
                out.writeObject(msg);
                if (msg != null && msg.getAction() != null) {
                    if (!msg.getAction().equals("RETRIEVE")) {
                        System.out.println("[C1] Client has sent msg to Leader: " + msg.getAction() + " " + msg.getKey() + " "
                                + msg.getValue());
                    } else {
                        System.out.println("[C1] Client has sent msg to Leader: " + msg.getAction() + " " + msg.getKey());
                    }
                } else if (msg != null) {
                    System.out.println("[C1] Client has requested for Leader IP address " + msg.getKey());
                }
            } else {
                Peer.disconnectedPeers.add(peerIpAddress);
            }
        } catch (NoRouteToHostException e) {
            System.out.println("4. Host has been disconnected.");
        } catch (ConnectException e) {
            System.out.println("[C1] Found a dead connection on IP Address " + peerIpAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * Receive a message from a peer
     *
     * @param s socket
     * @return PeerMessage
     */
    public synchronized PeerMessage receiveFrom(Socket s) {
        ObjectInputStream in;
        PeerMessage msg = null;
        try {
            if (s.isConnected()) {
                in = new ObjectInputStream(s.getInputStream());
                msg = (PeerMessage) in.readObject();
            } else {
                Peer.disconnectedPeers.add(s.getInetAddress().getHostAddress());
            }
        } catch (NoRouteToHostException e) {
            System.out.println("5. Host has been disconnected.");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("[C1] Handling Exception: Peer " + s.getInetAddress().getHostAddress() + " has closed the connection.");
            return msg;
        }
        return msg;
    }

    /**
     * Sets Leader IP Address
     *
     * @param msg incoming Leader info
     */
    public synchronized void setLeaderIpAddress(PeerMessage msg) {
        this.leaderIpAddress = msg.getKey();
    }
}
