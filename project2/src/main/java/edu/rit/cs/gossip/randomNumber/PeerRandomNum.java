package edu.rit.cs.gossip.randomNumber;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Implementation of a Peer in the Gossip Random Number Protocol
 */

public class PeerRandomNum {
    protected Integer randNum;
    public static List<String> peerAddresses = new ArrayList<>();
    final protected Map<Integer, List<String>> peerCache = new HashMap<>();
    final protected List<PeerStatus> alivePeers = new ArrayList<>();
    protected Random rand;
    protected Integer currentMax;
    String ipAddress;

    /**
     * Constructor for the Peer Random Number.
     */
    public PeerRandomNum() {
        try {
            this.rand = new Random();
            this.randNum = rand.nextInt(101);
            this.currentMax = randNum;
            List<String> ipAddresses = new ArrayList<>();
            InetAddress inetAddress = InetAddress.getLocalHost();
            ipAddress = inetAddress.getHostAddress();
            ipAddresses.add(ipAddress);
            peerCache.clear();
            peerCache.put(randNum, ipAddresses);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for peer cache.
     * @return
     */
    public synchronized Map<Integer, List<String>> getPeerCache() {
        return peerCache;
    }

    /**
     * Select a message to be sent
     * @return Selected message
     */
    public synchronized Map<Integer, List<String>> selectToSend() {
        return this.peerCache;
    }

    /**
     * Process the data from a local cache
     * @param incomingCache
     */
    public void processData(Map<Integer, List<String>> incomingCache) {
        synchronized (peerCache) {
            System.out.println("Starting to process the incoming cache of size " + incomingCache.size());
            for (Integer incomingRandNum : incomingCache.keySet()) {
                if (incomingRandNum.equals(currentMax)) {
                    List<String> addresses = incomingCache.get(incomingRandNum);
                    for (String ipAddress : addresses) {
                        if (!peerCache.get(incomingRandNum).contains(ipAddress)) {
                            peerCache.get(incomingRandNum).add(ipAddress);
                        }
                    }
                } else {
                    // update the peer cache to match the incoming cache
                    peerCache.remove(currentMax);
                    peerCache.put(incomingRandNum, incomingCache.get(incomingRandNum));
                    currentMax = incomingRandNum;
                }
            }
            // System.out.println("Completed processing the incoming cache.");
        }
    }

    /**
     * Determine whether to cache the message to a file
     * @param incomingRandNum random number received from the peer
     */
    public Boolean selectToKeep(Integer incomingRandNum) {
        if(!incomingRandNum.equals(currentMax)) {
            System.out.println("Comparing own current max random number " + currentMax + " to incoming random number " + incomingRandNum);
        }
        return incomingRandNum >= currentMax;
    }
}
