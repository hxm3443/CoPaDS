package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * This class handles one-to-all (broadcast) communication between peers. It is used for peer discovery in the
 * gossiping protocol.
 */

public class BroadcastingClient extends Thread {
    private DatagramSocket socket;
    private InetAddress broadcastingAddress;
    private String ipAddress;
    private byte[] buf;
    private boolean running;

    /**
     * Constructor for Broadcasting Client
     * @param ipAddress IP Address of the peer which joins the network
     * @throws Exception
     */
    public BroadcastingClient(String ipAddress) throws Exception {
        this.ipAddress = ipAddress;
        this.broadcastingAddress = InetAddress.getByName("255.255.255.255");
    }

    /**
     * This function is responsible for sending out the IP Address of the peer on the broadcast address.
     * @param msg IP Address of the peer which joins the network
     * @throws IOException
     */
    public void discoverPeers(String msg) throws IOException {
        initializeSocketForBroadcasting();
        copyMessageOnBuffer(msg);
        broadcastPacket(broadcastingAddress);
    }

    /**
     * This function initializes the Datagram socket and sets the socket as available for broadcasting the packet.
     * @throws SocketException
     */
    private void initializeSocketForBroadcasting() throws SocketException {
        socket = new DatagramSocket();
        socket.setBroadcast(true);
        System.out.println("[T1] The client socket has been initialized for broadcasting...");
    }

    /**
     * This function converts the IP Address message into bytes and writes it to a buffer.
     * @param msg IP Address of the newly joined peer
     */
    private void copyMessageOnBuffer(String msg) {
        buf = msg.getBytes();
        System.out.println("[T1] The message has been copied to the buffer. Msg is: " + msg);
    }

    /**
     * This function is responsible for broadcasting the packet on the broadcast address.
     * @param broadcastingAddress Broadcast Address
     * @throws IOException
     */
    private void broadcastPacket(InetAddress broadcastingAddress) throws IOException {
        DatagramPacket packet = new DatagramPacket(buf, buf.length, broadcastingAddress, 7896);
        socket.send(packet);
        System.out.println("[T1] Buffer has been sent on the broadcasting address: " + broadcastingAddress.toString());
    }

    /**
     * This function closes the socket.
     */
    public void close() {
        socket.close();
    }

    /**
     * This function periodically (every 2 seconds) broadcasts this peer's IP Address on the broadcast address.
     */
    public void run() {
        running = true;
        try {
            while (running) {
                discoverPeers(ipAddress);
                Thread.sleep(2000);
                // System.out.println("[T1] Broadcasting client is sleeping for 2 seconds.");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            running = false;
        } finally {
            this.close();
        }
    }
}
