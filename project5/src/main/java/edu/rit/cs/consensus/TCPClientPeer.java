package edu.rit.cs.consensus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;

/**
 * TCP Client Peer is the running implementation of a Client in the Raft Protocol.
 */
public class TCPClientPeer {
    ClientPeer clientPeer;
    String filename;

    /**
     * Constructor for TCP Client Peer
     *
     * @param clientPeer instance of clientPeer
     * @param filename   filename of the file containing client requests
     */
    public TCPClientPeer(ClientPeer clientPeer, String filename) {
        this.clientPeer = clientPeer;
        this.filename = filename;
    }

    /**
     * This function is responsible for determining the Leader, sending the client request to the Leader, as well as
     * receiving the acknowledgement/response from the Leader.
     */
    public void run() {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                PeerMessage requestLeader = clientPeer.selectLeaderRequest();
                Socket socket = clientPeer.sendTo(clientPeer.leaderIpAddress, requestLeader);
                PeerMessage leaderInfo = clientPeer.receiveFrom(socket);
                if (socket != null) {
                    socket.close();
                }
                clientPeer.setLeaderIpAddress(leaderInfo);
                System.out.println("[C2] LEADER IS " + clientPeer.leaderIpAddress);
                System.out.println("[C2] CLIENT REQUEST: " + line);
                String[] data = line.split(" ");
                String action = data[0];
                String key = data[1];
                Integer value = null;
                if (!action.equals("RETRIEVE")) {
                    value = Integer.parseInt(data[2]);
                }

                PeerMessage dataMessage = clientPeer.selectSendData(action, key, value);
                System.out.println("[C2] Data being sent to leader is " + dataMessage.toString());
                Socket s = clientPeer.sendTo(clientPeer.leaderIpAddress, dataMessage);

                PeerMessage leaderAcknowledgement = clientPeer.receiveFrom(s);
                if (action.equals("RETRIEVE")) {
                    System.out.println("[C2] " + leaderAcknowledgement.getKey() + " Value returned is " +
                            leaderAcknowledgement.getValue());
                } else {
                    System.out.println("[C2] " + leaderAcknowledgement.getKey());
                }

                if (s != null) {
                    s.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
