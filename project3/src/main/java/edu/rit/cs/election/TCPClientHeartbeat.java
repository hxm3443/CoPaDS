package edu.rit.cs.election;

import java.io.IOException;
import java.net.Socket;
import java.util.*;

/**
 * TCP Client is the running implementation of the PeerClient in the RAFT Protocol.
 */
public class TCPClientHeartbeat extends Thread {
    private Boolean running;
    private Peer peerClient;

    public TCPClientHeartbeat(Peer peerClient) {
        this.peerClient = peerClient;
    }

    /**
     * Initiates the thread responsible for handling the actions performed by the Leader peer.
     */
    public void run() {
        running = true;
        while (running) {
            try {
                if (peerClient.getState().equals("LEADER")) {
                    Set<Socket> peerConnections = new HashSet<>();
                    Set<String> peersSelected = peerClient.selectPeers();
                    PeerMessage peerMessage = peerClient.selectToSend();

                    for (String ipAddress : peersSelected) {
                        Socket socket = peerClient.sendTo(ipAddress, peerMessage);

                        if (socket != null) {
                            peerConnections.add(socket);
                        }
                    }

                    for (Socket s : peerConnections) {
                        PeerMessage msg = peerClient.receiveFrom(s);

                        if (msg == null) {
                            s.close();
                            continue;
                        }
                        System.out.println("[T4] Leader received the message " + msg.getMessage() +
                                " for election term " + msg.getElectionTerm());
                        s.close();
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}
