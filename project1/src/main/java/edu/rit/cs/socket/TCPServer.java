package edu.rit.cs.socket;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.rit.cs.word_count.AmazonFineFoodReview;
import edu.rit.cs.word_count.KV;
import edu.rit.cs.word_count.WordCount_Seq_Improved;

public class TCPServer {
    public static void main(String args[]) {
        try {
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    private static final String FILE_RECEIVED = "affr_recv.csv";
    private static final String FILE_OUTPUT = "output.txt";
    private static final byte EOF = '\0';

    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;

    public Connection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run() {
        try {
            // Receive and Save File from client
            FileOutputStream fos = new FileOutputStream(FILE_RECEIVED);
            byte[] buffer = new byte[1000];
            int readIndex;
            System.out.println("Waiting to read data from client.");
            while((readIndex = in.read(buffer)) != -1){
                if(buffer[readIndex - 1] == EOF) {
                    fos.write(buffer, 0, readIndex - 1);
                    break;
                }
                fos.write(buffer, 0, readIndex);
            }
            fos.close();
            System.out.println("Received and stored data from client into " + FILE_RECEIVED);

            // Read the received csv into a list of objects
            System.out.println("Reading the csv into a list of review objects.");
            List<AmazonFineFoodReview> allReviews = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(FILE_RECEIVED));
            String reviewLine;
            // skip the header line
            br.readLine();
            while((reviewLine = br.readLine()) != null) {
                AmazonFineFoodReview affr = new AmazonFineFoodReview(reviewLine);
                allReviews.add(affr);
            }
            br.close();

            System.out.println("Done creating a list of review objects. Starting word count.");

            List<KV<String, Integer>> mappedReviews = WordCount_Seq_Improved.map(allReviews);
            Map<String, Integer> reducedReviewsWordCount = WordCount_Seq_Improved.reduce(mappedReviews);

            // Write and Save the resulting output in the server
            System.out.println("Writing the resulting file: " + FILE_OUTPUT);
            fos = new FileOutputStream(FILE_OUTPUT);
            for(String key : reducedReviewsWordCount.keySet() ) {
                String entry = key + " : " + reducedReviewsWordCount.get(key) + "\n";
                fos.write(entry.getBytes());
            }
            System.out.println("Done writing the resulting file.");
            fos.close();

            // Sending the generated file from server to client
            FileInputStream fis = new FileInputStream(FILE_OUTPUT);
            BufferedInputStream bis = new BufferedInputStream(fis);
            DataInputStream dis = new DataInputStream(bis);

            int r;
            byte[] buf = new byte[1000];
            System.out.println("Starting to send " + FILE_OUTPUT + " data to the client.");
            while ((r = dis.read(buf)) != -1) {
                out.write(buf, 0, r);
            }
            out.write(EOF);

            fis.close();
            bis.close();
            dis.close();
            System.out.println("Done sending the data from server to client.");
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            try {
                out.close();
                in.close();
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
    }
}
