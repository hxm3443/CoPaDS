package edu.rit.cs.gossip.heartbeat;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Implementation of the Passive Peer for the Gossip Heartbeat Protocol.
 */
public class PeerHeartbeatServer extends PeerHeartbeat {

    /**
     * Send a selected message to a selected peer
     *
     * @param s   Selected peer
     * @param msg Selected message
     */
    public void sendTo(Socket s, PeerMessage msg) {
        ObjectOutputStream out;
        try {
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(msg);
            System.out.println("Done sending peer msg on to the socket from peer heartbeat server.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a message from any peer
     *
     * @return ServerSocket
     */
    public ServerSocket receiveFromAny() {
        ServerSocket listenSocket = null;
        try {
            int serverPort = 7896;
            listenSocket = new ServerSocket(serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listenSocket;
    }
}
