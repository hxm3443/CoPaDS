# **Project 1 - Docker for virtualization**

The purpose of this project is to learn about virtualization using containerization.

## **Code Description**
Client reads a csv file from terminal and sends it over to server over a socket.
Upon receiving this csv file, the server program performs word-count on this file and generates
a text file containing all results of the form <word, count>.
Server program then returns this result file to the client program for printing on the terminal.

The dataset file used in this project is downloaded from https://www.kaggle.com/snap/amazon-fine-food-reviews
and exists as affr.csv within amazon-fine-food-reviews subfolder (./amazon-fine-food-reviews/affr.csv).

The client-server program files can be found within the socket subfolder (./src/main/java/edu/rit/cs/socket).

The word-count program files can be found within the word_count subfolder (./src/main/java/edu/rit/cs/word_count).

## **Instructions to build and run using docker containers**

To start tcpserver and tcpclient and rebuild the docker image
```bash
docker-compose --file docker-compose-project1.yml up --build tcpserver tcpclient
```
After awhile you should see the following output
```bash
...
...
Successfully tagged csci251:latest
Recreating peer1 ... done
Creating tcpserver ... done
Creating tcpclient ... done
Attaching to tcpserver, tcpclient
tcpclient    | Initialize tcpclient...done!
tcpserver    | Initialize tcpserver...done!
```

### Start TCP server
Attach to the tcpserver container
```bash
docker exec -it tcpserver bash
```

Run the tcpserver program
```bash
java -cp target/project1-1.0-SNAPSHOT.jar edu.rit.cs.socket.TCPServer
```

### Start TCP client
Attach to the tcpclient container
```bash
docker exec -it tcpclient bash
```

Run the tcpclient program
```bash
java -cp target/project1-1.0-SNAPSHOT.jar edu.rit.cs.socket.TCPClient ./amazon-fine-food-reviews/affr.csv tcpserver
```

## **Sample Output**

Expected Server output
```bash
TCP Server is running and accepting client connections...
Waiting to read data from client.
Received and stored data from client into affr_recv.csv
Reading the csv into a list of review objects.
Done creating a list of review objects. Starting word count.
Writing the resulting file: output.txt
Done writing the resulting file.
Starting to send output.txt data to the client.
Done sending the data from server to client.
```

Expected Client output
```bash
Ready to read and send data to the server.
Start sending the file data to the server.
Done sending the file data.
Starting to receive and write the data to output_recv.txt
Done writing the result to output_recv.txt
Starting to read output_recv.txt and print it to the terminal.

#### Example output on the terminal:
Prints the result on the client side terminal in the format as shown below:
spilling : 3
unavailable : 17
vigirous : 1
.
.
.

Done printing the result file onto the terminal.
```


