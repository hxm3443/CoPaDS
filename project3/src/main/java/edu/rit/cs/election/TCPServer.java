package edu.rit.cs.election;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * TCP Server is the running implementation of the PeerServer in the RAFT Protocol.
 */
public class TCPServer extends Thread {
    private Boolean running;
    private Peer peerServer;

    public TCPServer(Peer peerServer) {
        this.peerServer = peerServer;
    }

    /**
     * Initiates the thread responsible for handling VoteRequest or AliveMessages received by a Peer.
     */
    public void run() {
        running = true;
        ServerSocket s = peerServer.receiveFromAny();
        Socket clientSocket = null;
        ObjectInputStream in;
        while (running) {
            try {
                if (s != null) {
                    clientSocket = s.accept();
                    in = new ObjectInputStream(clientSocket.getInputStream());
                    PeerMessage receivedMsg = (PeerMessage) in.readObject();

                    System.out.println("MESSAGE IN SERVER: " + receivedMsg.getMessage());

                    if (receivedMsg.getMessage().equals(Peer.REQUEST_VOTE_MSG)) {
                        System.out.println("[T5] Peer has received the request vote message.");

                        // If IncomingElectionTerm > CurrentElectionTerm
                        // If IncomingElectionTerm = CurrentElectionTerm
                        // If IncomingElectionTerm < CurrentElectionTerm
                        String voteToBeSent = "";
                        if (receivedMsg.getElectionTerm() > peerServer.getElectionTerm()) {
                            voteToBeSent = Peer.VOTE_MSG;
                            peerServer.setState("FOLLOWER");
                            peerServer.resetNumVotes();
                            peerServer.setElectionTerm(receivedMsg.getElectionTerm());
                            peerServer.setPotentialCandidate(false);
                        }
                        else if (receivedMsg.getElectionTerm() == peerServer.getElectionTerm()) {
                            if (peerServer.getState().equals("FOLLOWER") && !peerServer.isHasVoted()) {
                                voteToBeSent = Peer.VOTE_MSG;
                            }
                            else {
                                voteToBeSent = Peer.NO_VOTE_MSG;
                            }
                        }
                        else {
                            voteToBeSent = Peer.VOTE_MSG;
                        }
                        PeerMessage voteMessage = peerServer.selectVoteMessageToSend(voteToBeSent);
                        peerServer.sendTo(clientSocket, voteMessage);
                        peerServer.setHasVoted(true);
                    } else if (receivedMsg.getMessage().equals(Peer.ALIVE_MSG)) {
                        int currentElectionTerm = peerServer.getElectionTerm();
                        int incomingElectionTerm = receivedMsg.getElectionTerm();
                        System.out.println("[T5] Peer has received the alive message where current term is "
                                + currentElectionTerm + " and incoming term is " + incomingElectionTerm);
                        //CASE 1: Incoming election term > current election term - update term and state
                        //CASE 2: Incoming election term = current election term - update state
                        //CASE 3: Incoming election term < current election term - discard msg
                        boolean receivedMsgFromTrueLeader = false;
                        if (incomingElectionTerm > currentElectionTerm) {
                            peerServer.setElectionTerm(incomingElectionTerm);
                            peerServer.setState("FOLLOWER");
                            receivedMsgFromTrueLeader = true;
                        } else if (incomingElectionTerm == currentElectionTerm) {
                            peerServer.setState("FOLLOWER");
                            receivedMsgFromTrueLeader = true;
                        }

                        if (receivedMsgFromTrueLeader) {
                            peerServer.setLeaderLastTimeStamp(System.currentTimeMillis());
                            peerServer.setLeaderIpAddress(clientSocket.getInetAddress().getHostAddress());
                            peerServer.setReceivedLeaderMsg(true);
                            peerServer.setPotentialCandidate(false);
                            PeerMessage sendMessage = peerServer.selectToSend();
                            peerServer.sendTo(clientSocket, sendMessage);
                        }
                        else {
                            PeerMessage sendMessage = peerServer.selectToSend();
                            peerServer.sendTo(clientSocket, sendMessage);
                        }
                    }
                }
            }
            catch (EOFException e) {
                System.out.println("No info available to read.");
                try {
                    if (clientSocket != null) {
                        clientSocket.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                running = true;
            }
            catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                running = false;
            } finally {
                // Timer to check if this peer hasn't heard from the leader for 10 secs
                if (((System.currentTimeMillis() - peerServer.getLeaderLastTimeStamp())/1000) >= 10) {
                    System.out.println("Haven't heard from leader in over 10 seconds. Updating leader info.");
                    List<PeerInfoMessage> peerInfoMessages = Peer.peerInfo;
                    for (PeerInfoMessage infoMessage : peerInfoMessages) {
                        if (infoMessage.getIpAddress().equals(peerServer.getLeaderIpAddress())) {
                            synchronized (Peer.peerInfo) {
                                Peer.peerInfo.remove(infoMessage);
                            }
                            break;
                        }
                    }
                    peerServer.setLeaderLastTimeStamp(0L);
                    peerServer.setLeaderIpAddress("");
                    peerServer.setReceivedLeaderMsg(false);
                    peerServer.setPotentialCandidate(true);
                    peerServer.setTie(false);
                }
                System.out.println("[T5] Current Term " + peerServer.getElectionTerm() + " LEADER " + peerServer.getLeaderIpAddress());
            }
        }
    }
}