package edu.rit.cs.gossip.heartbeat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * TCP Server is the running implementation of the Passive Peer in the Gossip Heartbeat Protocol.
 */
public class TCPServer extends Thread {
    private Boolean running;
    PeerHeartbeatServer peerHeartbeatServer = new PeerHeartbeatServer();

    /**
     * Initiates the passive peer.
     */
    public void run() {
        running = true;
        ServerSocket s = peerHeartbeatServer.receiveFromAny();
        Socket clientSocket;
        ObjectInputStream in;
        while (running) {
            try {
                clientSocket = s.accept();
                System.out.println("[T4] TCP Server is initiating an input stream.");
                in = new ObjectInputStream(clientSocket.getInputStream());
                PeerMessage receivedMsg = (PeerMessage) in.readObject();
                PeerMessage sendMessage = peerHeartbeatServer.selectToSend();
                peerHeartbeatServer.sendTo(clientSocket, sendMessage);
                peerHeartbeatServer.selectToKeep(clientSocket.getInetAddress().getHostAddress());
                peerHeartbeatServer.processData(receivedMsg.getPeerCache());
                System.out.println("[T4] TCP Server is done with the task.");
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}