package edu.rit.cs.consensus;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * TCP Client Election is the running implementation of a Peer in Leader Election in the RAFT protocol.
 */
public class TCPClientElection extends Thread {
    private Peer peerClient;

    public TCPClientElection(Peer peerClient) {
        this.peerClient = peerClient;
    }

    /**
     * Randomly chooses an electionTimeout between 150ms and 300ms
     *
     * @param rounds sets the number of rounds
     * @return number of rounds or clock ticks to be performed
     */
    private int generateRandomTimeout(int rounds) {
        int min = 1000;  // milliseconds
        int max = 5000;  // milliseconds
        Random rand = new Random();
        int randInterval = rand.nextInt(max - min + 1);
        int timeout = min + randInterval;
        return timeout / rounds;
    }

    /**
     * Initiates the thread responsible for handling an Election. Sets the peer as a Follower, Candidate, or Leader
     * based on the Leader Election process in the RAFT protocol.
     */
    public void run() {
        // start election timeout timer (random number between 150ms and 300ms)
        // thread sleeps based on the timeout (timeout/5)
        // if this peer receives a message before hitting 0, set isPotentialCandidate to false and stop the timer, set
        // state to FOLLOWER, vote, set term to the one sent by the candidate and start a new timer
        // else if count down goes to 0, then set isPotentialCandidate to true and the state to CANDIDATE, starts a new
        // election term, and send voting requests and start a campaign

        // if this peer is a CANDIDATE, start a timer and receive from any until the timer goes to 0, keep incrementing
        // the vote count
        // For all peers, compare their vote counts, peer with the max num votes will become the leader
        // In the case when two candidates have the same max num votes, re-elect the leader

        // Check for how to ensure that the round/term number is synchronized with the max round num

        boolean running = true;

        // add a condition that checks if we already have a leader, then we don't run this loop
        while (running) {
            try {
                int electionTimeout = generateRandomTimeout(5);
                int countdown = 5;
                while (!peerClient.getState().equals("LEADER")) {
                    if (!peerClient.isTie()) {
                        if (!peerClient.isPotentialCandidate()) {
                            peerClient.setState("FOLLOWER");
                            break;
                        }

                        Thread.sleep(electionTimeout);
                        countdown--;

                        if (countdown <= 5) {
                            System.out.println("[T3] Current countdown is " + countdown);
                        }
                    }

                    if (countdown == 0) {
                        //hits timeout
                        peerClient.setPotentialCandidate(true);
                        peerClient.setState("CANDIDATE");
                        peerClient.resetNumVotes();
                        peerClient.incrementNumVotes();
                        peerClient.setHasVoted(true);

                        // Computing the max election term to ensure synchronicity
                        Integer maxTerm = 0;
                        for (PeerInfoMessage msg : Peer.peerInfo) {
                            if (msg.getElectionTerm() > maxTerm) {
                                maxTerm = msg.getElectionTerm();
                            }
                        }
                        peerClient.setElectionTerm(maxTerm + 1);

                        PeerMessage voterMessage = peerClient.selectVoterMessageToSend(peerClient.getElectionTerm());
                        Set<String> votersSelected = peerClient.selectPeers();
                        Set<Socket> voterConnections = new HashSet<>();

                        for (String voter : votersSelected) {

                            Socket socket = peerClient.sendTo(voter, voterMessage);
                            if (socket != null) {
                                voterConnections.add(socket);
                            }
                        }

                        int votingTimeout = generateRandomTimeout(10);
                        int counter = 5;

                        int votesCollected = 1;
                        int votesInFavor = 1;
                        while (counter != 0) {
                            Set<Socket> socketsToBeRemoved = new HashSet<>();

                            for (Socket s : voterConnections) {
                                PeerMessage msg = peerClient.receiveFrom(s);
                                if (msg == null) {
                                    continue;
                                }
                                String voteInfo = msg.getKey();
                                if (Peer.VOTE_MSG.equals(voteInfo)) {
                                    votesInFavor++;
                                    peerClient.incrementNumVotes();
                                    socketsToBeRemoved.add(s);
                                    votesCollected++;
                                } else if (Peer.NO_VOTE_MSG.equals(voteInfo)) {
                                    socketsToBeRemoved.add(s);
                                    votesCollected++;
                                } else {
                                    System.out.println("Did not receive a response from " + s.getInetAddress().getHostAddress());
                                }
                            }

                            for (Socket s : socketsToBeRemoved) {
                                voterConnections.remove(s);
                                s.close();
                            }

                            Thread.sleep(votingTimeout);
                            counter--;
                        }

                        // Voting is completed by this time
                        double voteRatio = votesInFavor * 1.0 / votesCollected;

                        if (voteRatio > 0.5) {
                            peerClient.setState("LEADER");
                            peerClient.setLeaderIpAddress(peerClient.getIpAddress());
                            System.out.println("[T3] We have a LEADER " + peerClient.getIpAddress());
                        } else if (voteRatio == 0.5) {
                            //System.out.println("SPLIT VOTE W/ NUM VOTES AND VOTES COLLECTED: " + votesInFavor
                            //        + "->" + votesCollected);
                            peerClient.setTie(true);
                            peerClient.resetNumVotes();
                            peerClient.setHasVoted(false);
                            peerClient.setPotentialCandidate(true);
                            peerClient.setReceivedLeaderMsg(false);
                            continue;
                        } else {
                            peerClient.setState("FOLLOWER");
                            System.out.println("[T3] The candidate switched to FOLLOWER.");
                        }

                        break;
                    }
                } //ELECTION ENDS

                peerClient.resetNumVotes();
                peerClient.setHasVoted(false);
                peerClient.setPotentialCandidate(true);
                peerClient.setTie(false);
                peerClient.setReceivedLeaderMsg(false);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}
