package edu.rit.cs.election;

import java.io.Serializable;
import java.util.Map;

/**
 * PeerMessage object to store the IP Address, Election Term, and Peer Cache.
 */
public class PeerMessage implements Serializable {
    private String message;
    private Integer electionTerm;
    private Map<String, Boolean> peerCache;

    public PeerMessage(String message, int electionTerm, Map<String, Boolean> peerCache) {
        this.message = message;
        this.electionTerm = electionTerm;
        this.peerCache = peerCache;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    public void setPeerCache(Map<String, Boolean> peerCache) {
        this.peerCache = peerCache;
    }

    public int getElectionTerm() {
        return electionTerm;
    }

    public void setElectionTerm(Integer electionTerm) {
        this.electionTerm = electionTerm;
    }
}