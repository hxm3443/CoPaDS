package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * This class acts as the Broadcasting Server, which is responsible for receiving the packet on the broadcast address
 * and adding each IP Address received.
 */

public class BroadcastingServer extends Thread {
    protected DatagramSocket socket;
    protected boolean running;
    protected byte[] buf = new byte[256];

    /**
     * Constructor for the Broadcasting server
     * @throws Exception
     */
    public BroadcastingServer() throws Exception {
        socket = new DatagramSocket(null);
        socket.setReuseAddress(true);
        socket.bind(new InetSocketAddress(7896));
    }

    /**
     * This function receives the packet on the broadcasting address and adds it's IP Address to
     * the overall list of IP Addresses.
     */
    public void run() {
        running = true;

        while (running) {
            try {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                // System.out.println("[T2] Broadcasting server awaiting to receive on the socket.");
                socket.receive(packet);
                // System.out.println("[T2] Broadcasting server has received data on the socket.");
                String receivedIpAddress = packet.getAddress().getHostAddress();
                // System.out.println("[T2] Broadcasting server has received the following address: " + receivedIpAddress);
                if (!PeerRandomNum.peerAddresses.contains(receivedIpAddress)) {
                    System.out.println("[T2] The following IP address will be added to the peer universe: " + receivedIpAddress);
                    PeerRandomNum.peerAddresses.add(receivedIpAddress);
                }
            } catch (IOException e) {
                e.printStackTrace();
                running = false;
            }
        }
        socket.close();
    }
}
