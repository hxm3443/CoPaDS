# **Project 2: Gossiping Protocol**

The purpose of this project is to get practice on sockets programming and develop the first p2p distributed system.

## **Gossiping with Resource Management: Gossiping for exchanging heartbeat messages**

### **Code Description**
Objective is to exchange heartbeat messages to detect a neighboring node has failed.
Periodically (every 2 seconds) exchange "I am alive." or "Hello" with other randomly selected neighboring peers as
described in the paper https://www.distributed-systems.net/my-data/papers/2007.osr.pdf.
Upon receiving these messages, cache them in a local buffer on a peer.
When we shutdown a peer, the heartbeat messages stop.
After 10 seconds, a peer detects a neighboring peer is down and populates a node-failure message to inform other peers.

The relevant code files can be found in ./src/main/java/edu/rit/cs/gossip/heartbeat folder.

## **Instructions to build and run using docker containers**

To start peer1, peer2, peer3, peer4, peer5 and rebuild the docker image
```bash
docker-compose --file docker-compose-project2.yml up --build peer1 peer2 peer3 peer4 peer5
```
After awhile you should see the following output
```bash
...
...
Successfully tagged csci251:latest
Recreating peer1 ... done
Recreating peer3 ... done
Recreating peer2 ... done
Recreating peer4 ... done
Recreating peer5 ... done
Attaching to peer1, peer3, peer2, peer4, peer5
peer1    | Initialize peer1...done!
peer2    | Initialize peer2...done!
peer3    | Initialize peer3...done!
peer4    | Initialize peer4...done!
peer5    | Initialize peer5...done!
```

### Start Peer1
Attach to the peer1 container
```bash
docker exec -it peer1 bash
```

Run the peer1 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.heartbeat.GossipHeartbeat
```

### Start Peer2
Attach to the peer2 container
```bash
docker exec -it peer2 bash
```

Run the peer2 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.heartbeat.GossipHeartbeat
```

### Start Peer3
Attach to the peer3 container
```bash
docker exec -it peer3 bash
```

Run the peer3 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.heartbeat.GossipHeartbeat
```

### Start Peer4
Attach to the peer4 container
```bash
docker exec -it peer4 bash
```

Run the peer4 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.heartbeat.GossipHeartbeat
```

### Start Peer5
Attach to the peer5 container
```bash
docker exec -it peer5 bash
```

Run the peer5 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.heartbeat.GossipHeartbeat
```




