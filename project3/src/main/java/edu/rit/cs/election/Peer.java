package edu.rit.cs.election;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.*;

/**
 * Implementation of a Peer in the Raft Protocol. A Peer could represent any one of the three states: Leader, Candidate,
 * or Follower.
 */
public class Peer {
    protected String state;
    protected Integer electionTerm;
    protected int numVotes;
    protected boolean isTie;
    protected boolean hasVoted;
    protected String leaderIpAddress;
    protected Long leaderLastTimeStamp;
    protected boolean isPotentialCandidate;
    protected boolean receivedLeaderMsg;
    protected static final String REQUEST_VOTE_MSG = "Requesting for a vote!";
    protected static final String VOTE_MSG = "Vote";
    protected static final String NO_VOTE_MSG = "No Vote";
    protected static final String ALIVE_MSG = "I am alive.";
    final protected static List<PeerInfoMessage> peerInfo = new ArrayList<>();
    final protected Map<String, Boolean> peerCache;
    protected Random random;

    /**
     * Constructor for Peer
     */
    public Peer() {
        this.state = "FOLLOWER";
        this.electionTerm = 0;
        this.numVotes = 0;
        this.leaderLastTimeStamp = 0L;
        this.isTie = false;
        this.hasVoted = false;
        this.isPotentialCandidate = true;
        this.receivedLeaderMsg = false;
        this.leaderIpAddress = "";
        this.peerCache = new HashMap<>();
        this.random = new Random();
    }

    public synchronized Long getLeaderLastTimeStamp() {
        return leaderLastTimeStamp;
    }

    public synchronized void setLeaderLastTimeStamp(Long leaderLastTime) {
        leaderLastTimeStamp = leaderLastTime;
    }

    public synchronized String getLeaderIpAddress() {
        return leaderIpAddress;
    }

    public synchronized void setLeaderIpAddress(String leaderIpAddr) {
        leaderIpAddress = leaderIpAddr;
    }

    public synchronized boolean isTie() {
        return isTie;
    }

    public synchronized void setTie(boolean tie) {
        isTie = tie;
    }

    /**
     * Getter for isPotentialCandidate
     *
     * @return isPotentialCandidate
     */
    public synchronized boolean isPotentialCandidate() {
        return isPotentialCandidate;
    }

    /**
     * Setter for setPotentialCandidate
     *
     * @param potentialCandidate sets the peer as a potential candidate (true) if it has not heard from any candidate
     *                           before its electionTime runs out otherwise not (false)
     */
    public synchronized void setPotentialCandidate(boolean potentialCandidate) {
        isPotentialCandidate = potentialCandidate;
    }

    /**
     * Gets the state of hasVoted
     *
     * @return true if the peer has voted
     */
    public synchronized boolean isHasVoted() {
        return hasVoted;
    }

    /**
     * Setter for hasVoted
     *
     * @param hasVotedIn sets it to true if the peer has voted for either itself or for another peer
     */
    public synchronized void setHasVoted(boolean hasVotedIn) {
        hasVoted = hasVotedIn;
    }

    /**
     * Resets number of votes to 0 for a new election
     */
    public synchronized void resetNumVotes() {
        numVotes = 0;
    }

    /**
     * Getter for numVotes
     *
     * @return number of votes received by a candidate peer
     */
    public synchronized int getNumVotes() {
        return numVotes;
    }

    /**
     * Increments number of votes as the peer receives vote messages from other peers
     */
    public synchronized void incrementNumVotes() {
        numVotes += 1;
    }

    /**
     * Getter for electionTerm
     *
     * @return election term of this peer
     */
    public synchronized Integer getElectionTerm() {
        return electionTerm;
    }

    /**
     * Setter for electionTerm
     *
     * @param electionTermIn sets the election term of this peer
     */
    public synchronized void setElectionTerm(Integer electionTermIn) {
        electionTerm = electionTermIn;
    }

    /**
     * Getter for the state of the peer
     *
     * @return one of three states: Leader, Candidate, or Follower
     */
    public synchronized String getState() {
        return state;
    }

    /**
     * Setter for the state of peer
     * @param stateIn sets the state as one of the following: Leader, Candidate, or Follower
     */
    public synchronized void setState(String stateIn) {
        state = stateIn;
    }

    /**
     * Getter for the peer cache
     *
     * @return peerCache
     */
    public synchronized Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    /**
     * Peer selects Vote Message to vote for a candidate if haven't voted already
     * Send a NO VOTE if you are also a candidate or you have voted already; otherwise
     * send VOTE if you haven't already voted
     *
     * @return PeerMessage containing vote, election term, and a null cache
     */
    public synchronized PeerMessage selectVoteMessageToSend(String voteMsg) {
        return new PeerMessage(voteMsg, electionTerm, null);
    }

    /**
     * Candidate requests a Vote Message from all peers
     *
     * @param electionTerm electionTerm of the candidate peer
     *
     * @return PeerMessage containing voteRequest, candidate's election term, and a null cache
     */
    public synchronized PeerMessage selectVoterMessageToSend(Integer electionTerm) {
        return new PeerMessage(REQUEST_VOTE_MSG, electionTerm, null);
    }

    /**
     * Select a message to be sent
     *
     * @return Selected message
     */
    public synchronized PeerMessage selectToSend() {
        return new PeerMessage(ALIVE_MSG, electionTerm, peerCache);
    }

    /**
     * Process the data from a local cache
     *
     * @param incomingCache The map containing ipAddress and status for peers.
     */
    public void processData(Map<String, Boolean> incomingCache) {
        synchronized (peerCache) {
            System.out.println("Starting to process the incoming cache.");
            for (String key : incomingCache.keySet()) {
                peerCache.put(key, incomingCache.get(key));
            }
            System.out.println("Completed processing the incoming cache.");
        }
    }

    /**
     * Determine whether to cache the message into peer cache.
     *
     * @param ipAddress IP Address
     */
    public void selectToKeep(String ipAddress) {
        synchronized (peerCache) {
            System.out.println("Adding the following ip address to the peer cache: " + ipAddress);
            peerCache.put(ipAddress, true);
        }
    }

    public synchronized boolean isReceivedLeaderMsg() {
        return receivedLeaderMsg;
    }

    public synchronized void setReceivedLeaderMsg(boolean receivedLeaderMsgIn) {
        receivedLeaderMsg = receivedLeaderMsgIn;
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param s   Selected peer
     * @param msg Selected message
     */
    public synchronized void sendTo(Socket s, PeerMessage msg) {
        ObjectOutputStream out;
        try {
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(msg);
            System.out.println("Done sending peer msg on to the socket from peer heartbeat server.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a message from any peer
     *
     * @return ServerSocket
     */
    public synchronized ServerSocket receiveFromAny() {
        ServerSocket listenSocket = null;
        try {
            int serverPort = 7896;
            listenSocket = new ServerSocket(serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listenSocket;
    }

    /**
     * Selects all the peers who joined the network to send voteRequests or aliveMessages
     *
     * @return a Set containing all peers from the PeerInfo list
     */
    public synchronized Set<String> selectPeers() {
        Set<String> peersSelected = new HashSet<>();
        String ipAddress = null;
        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        String peerAddress;
        // Sending to all peers except for itself
        for (PeerInfoMessage msg : peerInfo) {
            peerAddress = msg.getIpAddress();
            if (!peerAddress.equals(ipAddress)) {
                peersSelected.add(peerAddress);
            }
        }

        return peersSelected;
    }

    /**
     * Send a selected message to a selected peer
     *
     * @param ipAddress Selected peer
     * @param msg       Selected message
     */
    public synchronized Socket sendTo(String ipAddress, PeerMessage msg) {
        Socket s = null;
        ObjectOutputStream out;
        try {
            s = new Socket(ipAddress, 7896);
            System.out.println("Peer msg is " + msg.getElectionTerm() + " : " + msg.getMessage());
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(msg);
        }
        catch (ConnectException e) {
            this.peerCache.put(ipAddress, false);
            System.out.println("Found a dead connection on IP Address " + ipAddress);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * Receive a message from a peer
     *
     * @param s socket
     * @return PeerMessage
     */
    public synchronized PeerMessage receiveFrom(Socket s) {
        ObjectInputStream in;
        PeerMessage msg = null;
        try {
            System.out.println("Starting to receive peer msg on peer client.");
            in = new ObjectInputStream(s.getInputStream());
            System.out.println("Completed receiving peer msg on the peer client.");
            msg = (PeerMessage) in.readObject();
            System.out.println("Trying to fetch data " + msg.getMessage() +
                    " " + msg.getPeerCache());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Handling Exception: Peer " + s.getInetAddress().getHostAddress() + " has closed the connection.");
            return msg;
        }
        return msg;
    }
}
