package edu.rit.cs.gossip.heartbeat;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * TCP Client is the running implementation of the Active Peer in the Gossip Heartbeat Protocol.
 */
public class TCPClient extends Thread {
    private Boolean running;
    PeerHeartbeatClient peerHeartbeatClient = new PeerHeartbeatClient();
    final private Map<Socket, PendingSocketInfo> pendingSocketConnections = new HashMap<>();

    /**
     * Checks if the IP Address exists in pending socket connection
     * @param ipAddress
     * @return
     */
    private boolean isIpAddressInPendingSocketConnection(String ipAddress) {
        System.out.println("[T3] Checking if ip address " + ipAddress + " already has any pending connections.");
        for (PendingSocketInfo pendingSocketInfo : pendingSocketConnections.values()) {
            if (ipAddress.equals(pendingSocketInfo.getIpAddress())) {
                System.out.println("[T3] ip address " + ipAddress + " has a pending connection.");
                return true;
            }
        }
        System.out.println("[T3] ip address " + ipAddress + " does not have a pending connection.");
        return false;
    }

    /**
     * Initiates the active peer.
     */
    public void run() {
        running = true;
        while (running) {
            try {
                System.out.println("[T3] TCP Client is starting peer selection.");
                Set<String> peersSelected = peerHeartbeatClient.selectPeer();
                System.out.println("[T3] " + peersSelected.size() + " peers have been selected by TCP Client.");
                PeerMessage peerMessage = peerHeartbeatClient.selectToSend();
                System.out.println("[T3] Peer message: " + peerMessage.getMessage() + " " + peerMessage.getPeerCache().toString()
                        + " is ready to be sent by TCP Client.");
                for (String ipAddress : peersSelected) {
                    if (isIpAddressInPendingSocketConnection(ipAddress)) {
                        continue;
                    }
                    System.out.println("[T3] TCP Client is sending to ip address " + ipAddress);
                    Socket socket = peerHeartbeatClient.sendTo(ipAddress, peerMessage);
                    if (socket != null) {
                        // fetch timestamp after initiating contact with the passive thread
                        PendingSocketInfo pendingSocketInfo = new PendingSocketInfo(ipAddress, System.currentTimeMillis());
                        pendingSocketConnections.put(socket, pendingSocketInfo);
                    }
                }

                Set<Socket> sockets = pendingSocketConnections.keySet();
                Set<Socket> socketsToBeRemoved = new HashSet<>();
                for (Socket s : sockets) {
                    PeerMessage msg = peerHeartbeatClient.receiveFrom(s);
                    System.out.println("[T3] TCP Client has received from pending socket connection: " + s.toString());
                    Long timestampDiffInSeconds = (System.currentTimeMillis() - pendingSocketConnections.get(s).getTimestamp()) / 1000;
                    System.out.println("[T3] The timestamp diff in response for pending socket connection: "
                            + s.toString() + " is " + timestampDiffInSeconds);
                    if (timestampDiffInSeconds >= 10) {
                        // Check here if time elapsed is >= 10, then mark the peer as down; close socket
                        System.out.println("[T3] Found a dead connection from socket: " + pendingSocketConnections.get(s).getIpAddress());
                        peerHeartbeatClient.getPeerCache().put(pendingSocketConnections.get(s).getIpAddress(), false);
                        socketsToBeRemoved.add(s);
                        s.close();
                        continue;
                    } else if (msg == null) {
                        System.out.println("[T3] Socket : " + s.toString() + " has not received data yet however 10 seconds have not passed.");
                        continue;
                    }
                    System.out.println("[T3] Socket : " + s.toString() + " has received data within 10 seconds.");
                    peerHeartbeatClient.selectToKeep(pendingSocketConnections.get(s).getIpAddress());
                    peerHeartbeatClient.processData(msg.getPeerCache());
                    socketsToBeRemoved.add(s);
                    s.close();
                }

                //update pending socket connections
                for(Socket s : socketsToBeRemoved) {
                    pendingSocketConnections.remove(s);
                }
                System.out.println("Successfully removed " + socketsToBeRemoved.size() + " sockets from pending socket connections.");
                System.out.println("[T3] TCP Client is sleeping for 2 seconds.");
                Thread.sleep(2000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}
