package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * TCP Server is the running implementation of the Passive Peer in the Gossip Random Number Protocol.
 */

public class TCPServer extends Thread {
    private Boolean running;
    private PeerRandomNumServer peerRandomNumServer = new PeerRandomNumServer();

    /**
     * Initiates the passive peer.
     */
    public void run() {
        running = true;
        ServerSocket s = peerRandomNumServer.receiveFromAny();
        Socket clientSocket;
        ObjectInputStream in;
        while (running) {
            try {
                clientSocket = s.accept();
                in = new ObjectInputStream(clientSocket.getInputStream());
                // System.out.println("[T4] TCP Server is initiating an input stream.");
                Map<Integer, List<String>> incomingCache = (Map<Integer, List<String>>) in.readObject();
                Map<Integer, List<String>> outgoingCache = peerRandomNumServer.selectToSend();
                peerRandomNumServer.sendTo(clientSocket, outgoingCache);
                Integer incomingMax = incomingCache.keySet().iterator().next();
                Boolean shouldKeepNum = peerRandomNumServer.selectToKeep(incomingMax);
                if (Boolean.TRUE.equals(shouldKeepNum)) {
                    peerRandomNumServer.processData(incomingCache);
                }
                // System.out.println("[T4] TCP Server is done with the task.");
                Thread.sleep(1000);

            } catch (IOException | ClassNotFoundException | InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        }
    }
}
