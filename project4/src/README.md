# **Project 4: Raft protocol for Log Replication**

The purpose of this project is to get hands-on experience with leader election and agreement in Raft protocol.

## **Log Replication**

### **Code Description**
Replicate the key-value pair to all participating peers. When a client wants to update the value of a key. This value
should be propagated to all replicas correctly.
Delete all replicas when a client requests to remove a key-value pair.
Retrieve the value correctly when a client requests for it using a key.

The relevant code files can be found in ./src/main/java/edu/rit/cs/replication folder.

## **Instructions to build and run using docker containers**

To start peer1, peer2, peer3, peer4, peer5, client1, client2 and rebuild the docker image
```bash
docker-compose --file docker-compose-raft-replication.yml up --build peer1 peer2 peer3 peer4 peer5 client1 client2
```
After awhile you should see the following output
```bash
...
...
Successfully tagged csci251:latest
Recreating peer1 ... done
Recreating peer3 ... done
Recreating peer2 ... done
Recreating peer4 ... done
Recreating peer5 ... done
Recreating client1 ... done
Recreating client2 ... done
Attaching to peer1, peer2, peer3, peer4, peer5, client1, client2
peer1    | Initialize peer1...done!
peer2    | Initialize peer2...done!
peer3    | Initialize peer3...done!
peer4    | Initialize peer4...done!
peer5    | Initialize peer5...done!
client1  | Initialize client1...done!
client2  | Initialize client2...done!
```

### Start Peer1
Attach to the peer1 container
```bash
docker exec -it peer1 bash
```

Run the peer1 program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication
```

### Start Peer2
Attach to the peer2 container
```bash
docker exec -it peer2 bash
```

Run the peer2 program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication
```

### Start Peer3
Attach to the peer3 container
```bash
docker exec -it peer3 bash
```

Run the peer3 program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication
```

### Start Peer4
Attach to the peer4 container
```bash
docker exec -it peer4 bash
```

Run the peer4 program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication
```

### Start Peer5
Attach to the peer5 container
```bash
docker exec -it peer5 bash
```

Run the peer5 program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication
```

### To Start Client1
Attach to client1 container
```bash
docker exec -it client1 bash
```

Run the container program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication host_address src/main/java/edu/rit/cs/replication/actions1.txt
```
### To Start Client2
Attach to client2 container
```bash
docker exec -it client2 bash
```

Run the container program
```bash
java -cp target/project4-1.0-SNAPSHOT.jar edu.rit.cs.replication.RaftReplication host_address src/main/java/edu/rit/cs/replication/actions2.txt
```