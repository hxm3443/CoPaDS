package edu.rit.cs.gossip.heartbeat;

import java.io.Serializable;
import java.util.Map;

/**
 * PeerMessage object to store the IP Address and Peer Cache.
 */
public class PeerMessage implements Serializable {
    private String message;
    private Map<String, Boolean> peerCache;

    public PeerMessage(String message, Map<String, Boolean> peerCache) {
        this.message = message;
        this.peerCache = peerCache;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    public void setPeerCache(Map<String, Boolean> peerCache) {
        this.peerCache = peerCache;
    }
}