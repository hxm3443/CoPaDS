package edu.rit.cs.gossip.randomNumber;

import java.net.InetAddress;

/**
 * This class implements the Gossiping Random Number Protocol.
 */

public class GossipRandomNum {

    /**
     * Main entry point that initiates the threads.
     * @param args
     */
    public static void main(String[] args) {

        BroadcastingClient broadcastingClient = null;
        BroadcastingServer broadcastingServer = null;
        TCPClient tcpClient = null;
        TCPServer tcpServer = null;

        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            String ipAddress = inetAddress.getHostAddress();
            broadcastingClient = new BroadcastingClient(ipAddress);
            System.out.println("[T1] Gossip Random Number is starting the broadcasting client with address: " + ipAddress);
            broadcastingClient.start();

            broadcastingServer = new BroadcastingServer();
            // System.out.println("[T2] Gossip Random Number is starting the broadcasting server.");
            broadcastingServer.start();

            tcpClient = new TCPClient();
            // System.out.println("[T3] Gossip Random Number is starting the TCP Client.");
            tcpClient.start();

            tcpServer = new TCPServer();
            // System.out.println("[T4] Gossip Random Number is starting the TCP Server.");
            tcpServer.start();

        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted, Failed to complete operation");
            if (tcpServer != null) {
                System.out.println("TCP Server has been interrupted.");
                tcpServer.interrupt();
            }
            if (tcpClient != null) {
                System.out.println("TCP Client has been interrupted.");
                tcpClient.interrupt();
            }
            if (broadcastingClient != null) {
                System.out.println("Broadcasting Client has been interrupted.");
                broadcastingClient.interrupt();
            }
            if (broadcastingServer != null) {
                System.out.println("Broadcasting Server has been interrupted.");
                broadcastingServer.interrupt();
            }
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

