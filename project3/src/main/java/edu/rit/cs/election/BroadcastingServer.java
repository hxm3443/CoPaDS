package edu.rit.cs.election;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * This class acts as the Broadcasting Server, which is responsible for receiving the packet on the broadcast address
 * and adding each peer's PeerInfoMessage(electionTerm, ipAddress) received to a list.
 */

public class BroadcastingServer extends Thread {
    protected DatagramSocket socket;
    protected boolean running;
    protected byte[] buf = new byte[256];

    /**
     * Constructor for the Broadcasting server
     *
     * @throws Exception
     */
    public BroadcastingServer() throws Exception {
        socket = new DatagramSocket(null);
        socket.setReuseAddress(true);
        socket.bind(new InetSocketAddress(7896));
    }

    /**
     * This function receives the packet on the broadcasting address and adds the PeerInfoMessage(electionTerm, ipAddr)
     * to the overall list of PeerInfoMessage objects.
     */
    public void run() {
        running = true;
        while (running) {
            try {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                ByteArrayInputStream in = new ByteArrayInputStream(packet.getData());
                ObjectInputStream inputStream = new ObjectInputStream(in);
                PeerInfoMessage infoMessage = (PeerInfoMessage) inputStream.readObject();

                int flag = 0;
                for (PeerInfoMessage msg : Peer.peerInfo) {
                    if (infoMessage.getIpAddress().equals(msg.getIpAddress())) {
                        msg.setElectionTerm(infoMessage.getElectionTerm());
                        flag = 1;
                        break;
                    }
                }

                if (flag != 1) {
                    synchronized (Peer.peerInfo) {
                        Peer.peerInfo.add(infoMessage);
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                running = false;
            }
        }
        socket.close();
    }
}