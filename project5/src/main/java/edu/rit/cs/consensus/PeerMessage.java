package edu.rit.cs.consensus;

import java.io.Serializable;
import java.util.*;

/**
 * PeerMessage object to store the IP Address/Message, Action (if any), Election Term/Value, Peer Cache, Log file Info,
 * and Log file line count.
 */
public class PeerMessage implements Serializable {
    private String key;     // represents key for a peer/key for a client peer
    private String action;
    private Integer value;    // represents election term for a peer/value for a client peer
    private Map<String, Boolean> peerCache;
    private List<String> fileContent;
    private Integer lineCount;

    public PeerMessage(String key, String action, Integer value, Map<String, Boolean> peerCache, List<String> fileContent, Integer lineCount) {
        this.key = key;
        this.action = action;
        this.value = value;
        this.peerCache = peerCache;
        this.fileContent = fileContent;
        this.lineCount = lineCount;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, Boolean> getPeerCache() {
        return peerCache;
    }

    public void setPeerCache(Map<String, Boolean> peerCache) {
        this.peerCache = peerCache;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<String> getFileContent() {
        return fileContent;
    }

    public void setFileContent(List<String> fileContent) {
        this.fileContent = fileContent;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }
}
