package edu.rit.cs.gossip.randomNumber;

/**
 * PeerStatus object to store IP Address and Peer Status.
 */
public class PeerStatus {
    private String ipAddress;
    private Boolean isActive;

    public PeerStatus(String ipAddress, Boolean isActive) {
        this.ipAddress = ipAddress;
        this.isActive = isActive;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
