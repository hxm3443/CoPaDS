package edu.rit.cs.gossip.randomNumber;

/**
 * PendingSocketInfo object to store IP Address and Timestamp.
 */
public class PendingSocketInfo {
    private String ipAddress;
    private Long timestamp;

    public PendingSocketInfo(String ipAddress, Long timestamp) {
        this.ipAddress = ipAddress;
        this.timestamp =  timestamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
