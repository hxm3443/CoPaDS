# **Project 2: Gossiping Protocol**

The purpose of this project is to get practice on sockets programming and develop the first p2p distributed system.

## **Gossiping with Aggregating Computations: Gossiping for exchanging Random Number**

### **Code Description**
Objective is to determine which neighboring node has the largest randomly sampled number described in the paper
https://www.distributed-systems.net/my-data/papers/2007.osr.pdf.
In each round, each peer locally generates a random number (1-100).
Goal is to use Gossiping protocol to exchange these random numbers and determine a peer that has the largest number
in each round.

The relevant code files can be found in ./src/main/java/edu/rit/cs/gossip/randomNumber folder.

## **Instructions to build and run using docker containers**

To start peer1, peer2, peer3, peer4, peer5 and rebuild the docker image
```bash
docker-compose --file docker-compose-project2.yml up --build peer1 peer2 peer3 peer4 peer5
```
After awhile you should see the following output
```bash
...
...
Successfully tagged csci251:latest
Recreating peer1 ... done
Recreating peer3 ... done
Recreating peer2 ... done
Recreating peer4 ... done
Recreating peer5 ... done
Attaching to peer1, peer3, peer2, peer4, peer5
peer1    | Initialize peer1...done!
peer2    | Initialize peer2...done!
peer3    | Initialize peer3...done!
peer4    | Initialize peer4...done!
peer5    | Initialize peer5...done!
```

### Start Peer1
Attach to the peer1 container
```bash
docker exec -it peer1 bash
```

Run the peer1 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.randomNumber.GossipRandomNum
```

### Start Peer2
Attach to the peer2 container
```bash
docker exec -it peer2 bash
```

Run the peer2 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.randomNumber.GossipRandomNum
```

### Start Peer3
Attach to the peer3 container
```bash
docker exec -it peer3 bash
```

Run the peer3 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.randomNumber.GossipRandomNum
```

### Start Peer4
Attach to the peer4 container
```bash
docker exec -it peer4 bash
```

Run the peer4 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.randomNumber.GossipRandomNum
```

### Start Peer5
Attach to the peer5 container
```bash
docker exec -it peer5 bash
```

Run the peer5 program
```bash
java -cp target/project2-1.0-SNAPSHOT.jar edu.rit.cs.gossip.randomNumber.GossipRandomNum
```




