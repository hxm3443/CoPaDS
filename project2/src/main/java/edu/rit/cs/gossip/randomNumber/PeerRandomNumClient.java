package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of the Active Peer in the Gossip Random Number Protocol.
 */
public class PeerRandomNumClient extends PeerRandomNum {
    /**
     * Select a neighboring peer to send a message
     * @return Selected peer
     */
    public Set<String> selectPeer() {
        Set<String> peersSelected = new HashSet<>();
        int peerAddressesSize = peerAddresses.size();
        int groupSize = peerAddressesSize == 0 ? 0 : rand.nextInt(peerAddresses.size());
        int counter = 0;
        while (counter != groupSize) {
            int randIndex = rand.nextInt(peerAddresses.size());
            // Selecting alive peers only
            String ipAddress = peerAddresses.get(randIndex);

            if (alivePeers.size() != 0) {
                int selectPeer = 1;
                for (PeerStatus peerStatus : alivePeers) {
                    if (peerStatus.getIpAddress().equals(ipAddress) && Boolean.FALSE.equals(peerStatus.getActive())) {
                        System.out.println("Current peer " + ipAddress + " is dead as per local " +
                                "cache therefore not adding to peers selected.");
                        // skip the peer if it's status is dead in the cache
                        selectPeer = 0;
                        break;
                    }
                }
                if (selectPeer != 1) {
                    continue;
                }
            }
            InetAddress inetAddress = null;
            try {
                inetAddress = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            String ownIpAddress = inetAddress.getHostAddress();
            if (ipAddress.equals(ownIpAddress)) {
                System.out.println("Current peer " + ipAddress + " is actually me! i.e. " + ownIpAddress);
                continue;
            }
            System.out.println("Current peer " + ipAddress + " is added to peers selected.");
            peersSelected.add(ipAddress);
            counter++;
        }
        System.out.println("Total " + counter + " peers have been selected.");
        return peersSelected;
    }

    /**
     * Send a selected message to a selected peer
     * @param cache Selected peer
     */
    public Socket sendTo(String ipAddress, Map<Integer, List<String>> cache) {
        Socket s = null;
        ObjectOutputStream out;
        try {
            s = new Socket(ipAddress, 7896);
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(cache);
            System.out.println("The following message has been sent to " + ipAddress + " on the socket: " +
                    cache.toString());
        } catch (ConnectException e) {
            boolean foundEntry = false;
            for (PeerStatus peerStatus : alivePeers) {
                if (peerStatus.getIpAddress().equals(ipAddress)) {
                    peerStatus.setActive(false);
                    foundEntry = true;
                }
            }

            if(!foundEntry) {
                alivePeers.add(new PeerStatus(ipAddress, false));
            }
            System.out.println("Found a dead connection on IP Address " + ipAddress);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * Receive a message from a peer
     * @param s socket
     */
    public Map<Integer, List<String>> receiveFrom(Socket s) {
        ObjectInputStream in;
        Map<Integer, List<String>> incomingCache = null;
        try {
            // System.out.println("Starting to receive peer cache on peer random number client.");
            in = new ObjectInputStream(s.getInputStream());
            System.out.println("Completed receiving peer cache on the peer random number client with socket " + s.toString());
            incomingCache = (Map<Integer, List<String>>) in.readObject();
            System.out.println("Trying to fetch an incoming cache " + incomingCache.toString());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Handling Exception: Peer " + s.getInetAddress().getHostAddress() + " has closed the connection.");
            return incomingCache;
        }
        return incomingCache;
    }
}
