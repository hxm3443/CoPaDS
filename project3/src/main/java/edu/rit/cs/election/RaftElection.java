package edu.rit.cs.election;

import java.net.InetAddress;

/**
 * This class implements Leader Election in RAFT Protocol.
 */
public class RaftElection {

    /**
     * Main entry point that instantiates PeerClient and PeerServer as well as initiates all the threads.
     * @param args
     */
    public static void main(String[] args) {

        Peer peer = new Peer();
        BroadcastingClient broadcastingClient = null;
        BroadcastingServer broadcastingServer = null;
        TCPClientHeartbeat tcpClientHeartbeat = null;
        TCPClientElection tcpClientElection = null;
        TCPServer tcpServer = null;

        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            String ipAddress = inetAddress.getHostAddress();
            broadcastingClient = new BroadcastingClient(peer, ipAddress);
            System.out.println("[T1] Gossip Heartbeat is starting the broadcasting client.");
            broadcastingClient.start();

            broadcastingServer = new BroadcastingServer();
            System.out.println("[T2] Gossip Heartbeat is starting the broadcasting server.");
            broadcastingServer.start();

            tcpClientElection = new TCPClientElection(peer);
            System.out.println("[T3] Gossip Heartbeat is starting the TCP Client Election.");
            tcpClientElection.start();

            tcpClientHeartbeat = new TCPClientHeartbeat(peer);
            System.out.println("[T4] Gossip Heartbeat is starting the TCP Client Heartbeat.");
            tcpClientHeartbeat.start();

            tcpServer = new TCPServer(peer);
            System.out.println("[T5] Gossip Heartbeat is starting the TCP Server Heartbeat.");
            tcpServer.start();

        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted, Failed to complete operation");
            if (tcpClientElection != null) {
                tcpClientElection.interrupt();
            }
            if (tcpServer != null) {
                System.out.println("TCP Server has been interrupted.");
                tcpServer.interrupt();
            }
            if (tcpClientHeartbeat != null) {
                System.out.println("TCP Client has been interrupted.");
                tcpClientHeartbeat.interrupt();
            }
            if (broadcastingClient != null) {
                System.out.println("Broadcasting Client has been interrupted.");
                broadcastingClient.interrupt();
            }
            if(broadcastingServer != null) {
                System.out.println("Broadcasting Server has been interrupted.");
                broadcastingServer.interrupt();
            }
            Thread.currentThread().interrupt();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
