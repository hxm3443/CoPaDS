package edu.rit.cs.gossip.randomNumber;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the Passive Peer for the Gossip Random Number Protocol.
 */

public class PeerRandomNumServer extends PeerRandomNum {
    /**
     * Send a selected message to a selected peer
     * @param s   Selected peer
     * @param outgoingCache Selected message
     */
    public void sendTo(Socket s, Map<Integer, List<String>> outgoingCache) {
        ObjectOutputStream out;
        try {
            out = new ObjectOutputStream(s.getOutputStream());
            out.writeObject(outgoingCache);
            System.out.println("Done sending peer cache on to the socket from peer random number server.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a message from any peer
     */
    public ServerSocket receiveFromAny() {
        ServerSocket listenSocket = null;
        try {
            int serverPort = 7896;
            listenSocket = new ServerSocket(serverPort);
            // System.out.println("Peer random number server is running and awaiting connections...");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listenSocket;
    }
}
