# **Project 5: Raft protocol for Consensus**

The purpose of this project is to get hands-on experience with leader election and agreement in Raft protocol.

## **Consensus**

### **Code Description**
Maintain agreement on all nodes; this means these nodes have the same consistent values.
When one or more nodes go offline, their values will differ from the majority nodes.
When these offline nodes come back online, their values should be consistent with the majority nodes.
This can be achieved as a part of the recovery process when a new leader is detected.

The relevant code files can be found in ./src/main/java/edu/rit/cs/consensus folder.

## **Instructions to build and run using docker containers**

To start peer1, peer2, peer3, peer4, peer5, client1 and rebuild the docker image
```bash
docker-compose --file docker-compose-raft-consensus.yml up --build peer1 peer2 peer3 peer4 peer5 client1
```
After awhile you should see the following output
```bash
...
...
Successfully tagged csci251:latest
Recreating peer1 ... done
Recreating peer3 ... done
Recreating peer2 ... done
Recreating peer4 ... done
Recreating peer5 ... done
Recreating client1 ... done
Attaching to peer1, peer2, peer3, peer4, peer5, client1
peer1    | Initialize peer1...done!
peer2    | Initialize peer2...done!
peer3    | Initialize peer3...done!
peer4    | Initialize peer4...done!
peer5    | Initialize peer5...done!
client1  | Initialize client1...done!
```

### Start Peer1
Attach to the peer1 container
```bash
docker exec -it peer1 bash
```

Run the peer1 program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus
```

### Start Peer2
Attach to the peer2 container
```bash
docker exec -it peer2 bash
```

Run the peer2 program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus
```

### Start Peer3
Attach to the peer3 container
```bash
docker exec -it peer3 bash
```

Run the peer3 program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus
```

### Start Peer4
Attach to the peer4 container
```bash
docker exec -it peer4 bash
```

Run the peer4 program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus
```

### Start Peer5
Attach to the peer5 container
```bash
docker exec -it peer5 bash
```

Run the peer5 program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus
```

### To Start Client1
Attach to client1 container
```bash
docker exec -it client1 bash
```

Run the container program
```bash
java -cp target/project5-1.0-SNAPSHOT.jar edu.rit.cs.consensus.RaftConsensus host_address src/main/java/edu/rit/cs/consensus/actions1.txt
```

### To Disconnect a Container from the network
```bash
docker network disconnect copads_csci251network CONTAINER
```

### To Connect a Container to the network
```bash
docker network connect copads_csci251network CONTAINER
```

